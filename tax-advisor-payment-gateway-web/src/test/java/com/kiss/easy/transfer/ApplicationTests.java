package com.kiss.easy.transfer;

import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.service.tapg.master.CompanyService;
import com.kiss.pojo.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan( { "com.kiss" } )
public class ApplicationTests {

	@Autowired
	private CompanyService companyService;

	@Test
	public void testLoadCompany() {
		SearchResult<Company> companySearchResult = companyService.selectAllCompanies();
		System.out.println(companySearchResult.getList());
	}

}

