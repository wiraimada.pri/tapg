package com.kiss.easy.transfer.exception;

public class TapgException extends RuntimeException {
    public TapgException(String message) {
        super(message);
    }

    public TapgException(String message, Throwable cause) {
        super(message, cause);
    }

    public static final String INVALID_DATA_VERSION = "DATA VERSION DID NOT MATCHED.";
}
