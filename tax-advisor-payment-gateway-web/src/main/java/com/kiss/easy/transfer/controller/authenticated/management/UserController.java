package com.kiss.easy.transfer.controller.authenticated.management;

import com.kiss.easy.transfer.constant.FieldConstant;
import com.kiss.easy.transfer.controller.authenticated.base.BaseManagementController;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.master.UserRole;
import com.kiss.easy.transfer.enumeration.MenuEnum;
import com.kiss.easy.transfer.service.tapg.master.UserRoleService;
import com.kiss.easy.transfer.service.tapg.master.UserService;
import com.kiss.easy.transfer.service.tapg_admin.TaCompanyService;
import com.kiss.easy.transfer.util.FormValidationUtil;
import com.kiss.pojo.DataTablesSearchResult;
import com.kiss.pojo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Controller
public class UserController extends BaseManagementController {
    /********************************************** - SERVICE SECTION - **********************************************/
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private TaCompanyService taCompanyService;

    /********************************************** - AUTO INJECT SECTION - **********************************************/
    @Autowired
    private FormValidationUtil formValidationUtil;

    /********************************************** - ROOT PATH SECTION - **********************************************/
    private static final String PAGE_DIRECTORY = PAGE_BASE + "user/";
    public static final String PATH_ROOT = PATH_BASE + "user";

    /********************************************** - LIST SECTION - **********************************************/
    public static final String PAGE_LIST = PAGE_DIRECTORY + "list";
    public static final String PATH_LIST = PATH_ROOT;
    private static final String REQUEST_MAPPING_LIST = PATH_LIST + "*";
    public static final String COMMAND_LIST = "redirect:" + PATH_LIST;

    @GetMapping({PATH_LIST, REQUEST_MAPPING_LIST})
    public String doShowListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");
        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("activeMenu", MenuEnum.MANAGEMENT_USER.id());

        return getPageContent(PAGE_LIST);
    }

    /********************************************** - AJAX SEARCH LIST SECTION - **********************************************/
    public static final String PATH_AJAX_SEARCH_LIST = PATH_ROOT + "/search";
    private static final String REQUEST_MAPPING_SEARCH_LIST = PATH_AJAX_SEARCH_LIST + "*";

    @PostMapping({REQUEST_MAPPING_SEARCH_LIST})
    public void doSearchDataTablesList(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesSearchResult dataTablesSearchResult = new DataTablesSearchResult();
            try {
                dataTablesSearchResult.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesSearchResult.setDraw(0);
            }
            int offset = 0;
            int limit = 50;
            try {
                offset = Math.abs(Integer.parseInt(request.getParameter("start")));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Math.abs(Integer.parseInt(request.getParameter("length")));
            } catch (NumberFormatException e) {
            }

            List<String[]> data = new ArrayList<>();
            SearchResult<User> searchResult = userService.selectAllUsersByLoggedInUser();
            if (searchResult.getCount() > 0) {
                dataTablesSearchResult.setRecordsFiltered(searchResult.getCount());
                dataTablesSearchResult.setRecordsTotal(searchResult.getCount());

//              <tr>
//					<td class="text-center">1.</td>
//					<td>Cindy</td>
//					<td>Cindy</td>
//					<td>cindy@gmail.com</td>
//					<td class="text-center">
//
//
//					</td>
//				</tr>

                String labelRole = getMessage("label.role");
                String labelEdit = getMessage("label.edit");
                String labelChangePassword = getMessage("label.password.change");
                int i = offset;
                for (User result : searchResult.getList()) {
                    data.add(new String[]{
                            "" + (++i),
                            result.getUsername(),
                            result.getName(),
                            result.getEmail(),
                            (
                                    "<a href=\"" + getPathURL(PATH_SET_ROLE + "/" + result.getId()) + "\" class=\"btn btn-sm btn-default\">" + labelRole + "</a> " +
                                            "<a href=\"" + getPathURL(PATH_EDIT + "/" + result.getId()) + "\" class=\"btn btn-sm btn-warning\">" + labelEdit + "</a> " +
                                            "<a href=\"" + getPathURL(PATH_CHANGE_PASSWORD + "/" + result.getId()) + "\" class=\"btn btn-sm btn-primary\">" + labelChangePassword + "</a>"
                            )
                    });
                }
            }
            dataTablesSearchResult.setData(data);

            writeJsonResponse(response, dataTablesSearchResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /********************************************** - ADD SECTION - **********************************************/
    public static final String PAGE_ADD = PAGE_DIRECTORY + "add";
    public static final String PATH_ADD = PATH_ROOT + "/add";
    private static final String REQUEST_MAPPING_ADD = PATH_ADD + "*";

    @GetMapping(REQUEST_MAPPING_ADD)
    public String doShowAddPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");

        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }

        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("form", new User());
        model.addAttribute("companyCode", taCompanyService.selectCurrentTaCompany().getCode() + "_");

        return getPageContent(PAGE_ADD);
    }

    @PostMapping(value = REQUEST_MAPPING_ADD)
    public String doAdd(@ModelAttribute("form") final User form, final BindingResult errors, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        formValidationUtil.validatingInput(errors, "username", form.getUsername(), getMessage("label.username"), FieldConstant.MAX_LENGTH_USERNAME);
        formValidationUtil.validatingInput(errors, "password", form.getUsername(), getMessage("label.password"), FieldConstant.MAX_LENGTH_PASSWORD);
        formValidationUtil.validatingInput(errors, "name", form.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);
        formValidationUtil.validatingEmail(errors, "email", form.getEmail(), getMessage("label.email"), FieldConstant.MAX_LENGTH_EMAIL);

        if (!errors.hasErrors()) {
//            ========================INSERT NEW DATA=========================
            try {
                userService.insertUser(form);
                model.addAttribute("okMessage", getMessage("message.ok.data"));
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("errorMessage", errMessage);
            }
        }

        return getPageContent(PAGE_ADD);
    }

    /********************************************** - CHANGE PASSWORD SECTION - **********************************************/
    public static final String PAGE_CHANGE_PASSWORD = PAGE_DIRECTORY + "change-password";
    public static final String PATH_CHANGE_PASSWORD = PATH_ROOT + "/change-password";
    private static final String REQUEST_MAPPING_CHANGE_PASSWORD = PATH_CHANGE_PASSWORD + "/{id}";
    public static final String COMMAND_CHANGE_PASSWORD = "redirect:" + PATH_CHANGE_PASSWORD;

    @RequestMapping(value = REQUEST_MAPPING_CHANGE_PASSWORD, method = {RequestMethod.GET})
    public String doShowChangePasswordPage(final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        User originalUser = userService.selectOneUserById(id);

        if (originalUser == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.user.edit.invalid");
        }
        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("data", originalUser);

        return getPageContent(PAGE_CHANGE_PASSWORD);
    }

    @RequestMapping(value = REQUEST_MAPPING_CHANGE_PASSWORD, method = {RequestMethod.POST})
    public String doChangePassword(@ModelAttribute("user") final User user, final BindingResult errors, final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        User userOriginal = userService.selectOneUserById(id);

        if ((userOriginal == null) || (user.getRowVersion() != userOriginal.getRowVersion())) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.user.edit.invalid");
        }

        user.setId(userOriginal.getId());

        formValidationUtil.validatingMandatoryInput(errors, "newPassword", user.getNewPassword(), getMessage("label.password.new", FieldConstant.MAX_LENGTH_PASSWORD));
        formValidationUtil.validatingMandatoryInput(errors, "confirmPassword", user.getConfirmPassword(), getMessage("label.password.confirmation", FieldConstant.MAX_LENGTH_PASSWORD));
        formValidationUtil.validatingMatchedValue(errors, "newPassword", user.getNewPassword(), "label.password.new", user.getConfirmPassword(), "label.password.confirmation");

        if (!errors.hasErrors()) {
            try {
                userService.changePassword(user);

                String okMessage = getMessage("form.ok.master.user.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", userOriginal.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_CHANGE_PASSWORD + "/" + userOriginal.getId());
            }
        } else return getPageContent(PAGE_CHANGE_PASSWORD) + "?errorMessage=" + getMessage("message.error.data");
    }

    /********************************************** - EDIT SECTION - **********************************************/
    public static final String PAGE_EDIT = PAGE_DIRECTORY + "edit";
    public static final String PATH_EDIT = PATH_ROOT + "/edit";
    private static final String REQUEST_MAPPING_EDIT = PATH_EDIT + "/{id}";
    public static final String COMMAND_EDIT = "redirect:" + PATH_EDIT;

    @RequestMapping(value = REQUEST_MAPPING_EDIT, method = {RequestMethod.GET})
    public String doShowEditPage(final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        User originalUser = userService.selectOneUserById(id);

        if (originalUser == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.user.edit.invalid");
        }
        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

//        model.addAttribute("listMInfrastructure", managementInfrastructureService.selectAllManagementInfrastructure(null, null, null, null, null, null));
        model.addAttribute("data", originalUser);

        return getPageContent(PAGE_EDIT);
    }

    @RequestMapping(value = REQUEST_MAPPING_EDIT, method = {RequestMethod.POST})
    public String doEdit(@ModelAttribute("user") final User user, final BindingResult errors, final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        User userOriginal = userService.selectOneUserById(id);

        if ((userOriginal == null) || (user.getRowVersion() != userOriginal.getRowVersion())) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.user.edit.invalid");
        }

        user.setId(userOriginal.getId());

//        formValidationUtil.validatingInput(errors, "username", user.getUsername(), getMessage("label.username"), FieldConstant.MAX_LENGTH_USERNAME);
        formValidationUtil.validatingInput(errors, "name", user.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);
        formValidationUtil.validatingEmail(errors, "email", user.getEmail(), getMessage("label.email"), FieldConstant.MAX_LENGTH_EMAIL);

        if (!errors.hasErrors()) {
            try {
                userOriginal.setName(user.getName());
                userOriginal.setEmail(user.getEmail());
                userOriginal.setEnabled(user.isEnabled());
                userOriginal.setAccountNonLocked(user.isAccountNonLocked());
                userOriginal.setAccountNonExpired(user.isAccountNonExpired());
                userOriginal.setCredentialsNonExpired(user.isCredentialsNonExpired());
                userService.updateUser(userOriginal);

                String okMessage = getMessage("form.ok.master.user.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", userOriginal.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_EDIT);
            }
        } else return getPageContent(PAGE_EDIT);
    }

    /********************************************** - SET-ROLE SECTION - **********************************************/
    public static final String PAGE_SET_ROLE = PAGE_DIRECTORY + "set-role";
    public static final String PATH_SET_ROLE = PATH_ROOT + "/set-role";
    public static final String REQUEST_MAPPING_SET_ROLE = PATH_SET_ROLE + "/{id}";

    @RequestMapping(value = REQUEST_MAPPING_SET_ROLE, method = {RequestMethod.GET})
    public String doShowSetRolePage(final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        User originalUser = userService.selectOneUserById(id);

        if (originalUser == null) {
            throw new Exception();
        }
        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        List<UserRole> userRoleTemp = userRoleService.selectAllUserRolesForLoggedInUser();
        if (originalUser.getRoleIds().size() > 0) {
            for (UserRole ur : userRoleTemp) {
                ur.setSelected(originalUser.getRoleIds().contains(ur.getRole().getId()));
            }
        }

        originalUser.setUserRoles(userRoleTemp);
        model.addAttribute("form", originalUser);

        return getPageContent(PAGE_SET_ROLE);
    }

    @RequestMapping(value = REQUEST_MAPPING_SET_ROLE, method = {RequestMethod.POST})
    public String doSetRole(@ModelAttribute("form") final User user, final BindingResult errors, final Model model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        String id = request.getParameter("id");
        User originalUser = userService.selectOneUserById(id);

        if (originalUser == null) {
            throw new Exception();
        }
        if (user.getRowVersion() != originalUser.getRowVersion()) {
            model.addAttribute("errorMessage", getMessage("form.error.user.edit.invalid"));
            return getPageContent(PAGE_LIST);
        }

        user.setId(originalUser.getId());
        String[] roleArr = request.getParameterValues("roleArr[]");

        if (!errors.hasErrors()) {
            try {
                userRoleService.updateUserRole(user, roleArr);

                String okMessage = getMessage("form.ok.master.user.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", originalUser.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_SET_ROLE);
            }
        } else {
            model.addAttribute("errorMessage", getMessage("message.error.data"));
            return getPageContent(PAGE_SET_ROLE);
        }

    }

    /********************************************** - GLOBAL MODEL ATTRIBUTE SECTION - **********************************************/
    @ModelAttribute
    public void addModelAttribute(final Model model, final HttpServletRequest request) {
        if (!isAjaxRequest(request)) {
            model.addAttribute("title", getMessage("page.title.management.user"));
        }
    }
}
