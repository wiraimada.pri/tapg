package com.kiss.easy.transfer.service.tapg.master;

import com.kiss.easy.transfer.config.PasswordEncoderConfig;
import com.kiss.easy.transfer.dao.tapg.master.RoleMenuDao;
import com.kiss.easy.transfer.dao.tapg.master.UserDao;
import com.kiss.easy.transfer.dao.tapg.master.UserRoleDao;
import com.kiss.easy.transfer.dao.tapg_admin.master.TaCompanyDao;
import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.RoleMenu;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.enumeration.UserTypeEnum;
import com.kiss.easy.transfer.service.tapg.BaseService;
import com.kiss.pojo.SearchResult;
import com.kiss.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Service
public class UserService extends BaseService implements UserDetailsService {
	@Autowired
	private DateUtil dateUtil;
	@Autowired
	private PasswordEncoderConfig passwordEncoderConfig;
	@Autowired
	private RoleMenuDao roleMenuDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private TaCompanyDao taCompanyDao;

	public long countUsers() {
		return userDao.count();
	}

	public long countUsersByTaCompanyId(String taCompanyId) {
		return userDao.countByTaCompanyId(taCompanyId);
	}

	@Transactional
	public void changePassword(User user) {
		User userOriginal = selectOneUserById(user.getId());
		prepareUpdate(userOriginal);
		if (userOriginal == null) {
			throw new DataIntegrityViolationException(HttpStatus.NOT_FOUND.getReasonPhrase());
		}

		userOriginal.setPassword(passwordEncoderConfig.getPasswordEncoder().encode(user.getNewPassword()));

		userDao.updateUser(userOriginal);
	}

	@Transactional
	public int deleteUser(String id) {
		return userDao.deleteUser(id);
	}

	@Transactional
	public User insertUser(User user) {
		super.prepareInsert(user);

		user.setPassword(passwordEncoderConfig.getPasswordEncoder().encode(user.getPassword()));
		user.setUserType(UserTypeEnum.USER.value());
		user.setTaCompany(getLoggedUser().getTaCompany());
		user.setUsername(taCompanyDao.selectOne(user.getTaCompany().getId()).getCode() + "_" + user.getUsername());
		user.setCredentialsNonExpired(Boolean.TRUE);
		user.setAccountNonExpired(Boolean.TRUE);
		user.setAccountNonLocked(Boolean.TRUE);
		user.setEnabled(Boolean.TRUE);
		userDao.insertUser(user);
		return user;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return (UserDetails) userDao.selectOneByUsername(username);
	}

	public SearchResult<User> selectAllUsers() {
		SearchResult<User> searchResult = new SearchResult<>();
		searchResult.setCount((int) userDao.count());
		if (searchResult.getCount() > 0) {
			searchResult.setList(userDao.selectAll());
		}
		return searchResult;
	}

	public SearchResult<User> selectAllUsersByTaCompanyId(String taCompanyId) {
		SearchResult<User> searchResult = new SearchResult<>();
		searchResult.setCount((int) userDao.countByTaCompanyId(taCompanyId));
		if (searchResult.getCount() > 0) {
			searchResult.setList(userDao.selectAllByTaCompanyId(taCompanyId));
		}
		return searchResult;
	}

	public List<Role> selectAllRoles(User user) {
		return userRoleDao.selectAllRoles(user);
	}

	public List<RoleMenu> selectAllRoleMenuList(List<Role> roleList) {
		return roleMenuDao.selectAll(roleList);
	}

	public SearchResult<User> selectAllUsersByLoggedInUser() {
		return selectAllUsersByTaCompanyId(getLoggedUser().getTaCompany().getId());
	}

	public User selectOneUserById(String id) {
		User user = userDao.selectOneById(id);
		user.setTaCompany(taCompanyDao.selectOne(user.getTaCompany().getId()));
//		if (userRoleDao.countUserRolesByUserId(user.getId()) > 0) {
			user.setUserRoles(userRoleDao.selectAllUserRolesByUserId(user.getId()));
//		}
		return user;
	}

	@Transactional
	public void updateUser(User user) {
		prepareUpdate(user);
		userDao.updateUser(user);
	}
}
