package com.kiss.easy.transfer.interceptor;

import com.kiss.util.DateUtil;
import com.kiss.web.thymeleaf.interceptor.KissInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 1/20/19.
 */
@Component("commonInterceptor")
public class CommonInterceptor extends KissInterceptor {
	@Autowired
	private DateUtil dateUtil;

	@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
		if (response.isCommitted()) {
			return;
		}

		if (modelAndView != null && dateUtil != null) {
			modelAndView.addObject("copyrightYear", dateUtil.getYear());
		}
	}

	@Override
	public String[] getAddPathPatterns() {
		return new String[] {"/**"};
	}

	@Override
	public String[] getExcludePathPatterns() {
		return null;
	}

	@Override
	public PathMatcher getPathMatcher() {
		return null;
	}
}
