package com.kiss.easy.transfer.service.tapg.master;

import com.kiss.easy.transfer.dao.tapg.master.CompanyDao;
import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.service.tapg.BaseService;
import com.kiss.pojo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by NEVIIM - DIX on 11/01/2019.
 */
@Service
public class CompanyService extends BaseService {
	@Autowired
	private CompanyDao companyDao;

	public SearchResult<Company> selectAllCompanies() {
		SearchResult<Company> searchResult = new SearchResult<>();
		searchResult.setCount((int) companyDao.count());
		if (searchResult.getCount() > 0) {
			searchResult.setList(companyDao.selectAll());
		}
		return searchResult;
	}

	public SearchResult<Company> selectAllActiveCompaniesByLoggedInUser() {
		return selectAllActiveCompaniesByTaCompany(getLoggedUser().getTaCompany().getId());
	}

	public SearchResult<Company> selectAllActiveCompaniesByTaCompany(String taCompanyId) {
		SearchResult<Company> searchResult = new SearchResult<>();
		searchResult.setCount((int) companyDao.countActiveCompanyByTaCompanyId(taCompanyId));
		if (searchResult.getCount() > 0) {
			searchResult.setList(companyDao.selectAllActiveCompanyByTaCompanyId(taCompanyId));
		}
		return searchResult;
	}

	public SearchResult<Company> selectAllCompaniesByLoggedInUser() {
		return selectAllCompaniesByTaCompany(getLoggedUser().getTaCompany().getId());
	}

	public SearchResult<Company> selectAllCompaniesByTaCompany(String taCompanyId) {
		SearchResult<Company> searchResult = new SearchResult<>();
		searchResult.setCount((int) companyDao.countCompanyByTaCompanyId(taCompanyId));
		if (searchResult.getCount() > 0) {
			searchResult.setList(companyDao.selectAllCompanyByTaCompanyId(taCompanyId));
		}
		return searchResult;
	}

	public Company selectOneCompanyById(String id) {
		return companyDao.selectOneById(id);
	}

	@Transactional
	public void insertCompany(Company company) {
		super.prepareInsert(company);
		company.setTaCompany(getLoggedUser().getTaCompany());
		companyDao.insertCompany(company);
	}

	@Transactional
	public void updateCompany(Company company) {
		super.prepareUpdate(company);
		companyDao.updateCompany(company);
	}

	public long countCompany() {
		return companyDao.count();
	}

	@Transactional
	public void deleteCompany(String id) {
		companyDao.deleteCompany(id);
	}
}
