package com.kiss.easy.transfer.controller.authenticated.transaction;

import com.kiss.easy.transfer.constant.FieldConstant;
import com.kiss.easy.transfer.constant.StringConstant;
import com.kiss.easy.transfer.controller.authenticated.base.BaseTransactionController;
import com.kiss.easy.transfer.dao.tapg_admin.master.TaCompanyDao;
import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayroll;
import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayrollDetail;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.easy.transfer.service.tapg.master.CompanyService;
import com.kiss.easy.transfer.service.tapg.master.UserService;
import com.kiss.easy.transfer.service.tapg.transaction.CompanyPayrollService;
import com.kiss.easy.transfer.util.FormValidationUtil;
import com.kiss.pojo.DataTablesSearchResult;
import com.kiss.pojo.SearchResult;
import com.kiss.util.DateUtil;
import com.kiss.web.thymeleaf.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Controller
public class CompanyPayrollController extends BaseTransactionController {

    /************************ AUTOWIRED STUFF ************************/
    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private CompanyPayrollService companyPayrollService;

    @Autowired
    private TaCompanyDao taCompanyDao;

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private FormValidationUtil formValidationUtil;
    /************************ FILE DIRECTORY *********************/
    @Value("${upload.file.path:}")
    private String uploadFilePath;

    /************************ STATIC PATH *********************/
    private static final String PATH_DIRECTORY = BaseTransactionController.PATH_TRANSACTION;

    private static final String PAGE_DIRECTORY = BaseTransactionController.PAGE_TRANSACTION;

    private static final String PAGE_PAYROLL_UPLOAD = PAGE_DIRECTORY + "add";

    public static final String PATH_PAYROLL_UPLOAD = PATH_DIRECTORY + "add";

    public static final String REQUEST_MAPPING_PAYROLL = PATH_PAYROLL_UPLOAD + "*";

    public static final String PAGE_LIST = PAGE_DIRECTORY + "list";

    public static final String PATH_LIST = PATH_DIRECTORY + "list";

    //    /********************************************** - AJAX SEARCH LIST SECTION - **********************************************/
    public static final String PATH_AJAX_SEARCH_LIST = PATH_DIRECTORY + "search";
    public static final String PATH_AJAX_LIST = PAGE_DIRECTORY + "search-list";

    private static final String REQUEST_MAPPING_AJAX_SEARCH_LIST = PATH_AJAX_SEARCH_LIST + "*";

    @RequestMapping(value = REQUEST_MAPPING_AJAX_SEARCH_LIST, method = {RequestMethod.POST})
    public void doSearchDataTablesList(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesSearchResult dataTablesSearchResult = new DataTablesSearchResult();
            try {
                dataTablesSearchResult.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesSearchResult.setDraw(0);
            }

            int offset = 0;
            int limit = 50;
            try {
                offset = Math.abs(Integer.parseInt(request.getParameter("start")));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Math.abs(Integer.parseInt(request.getParameter("length")));
            } catch (NumberFormatException e) {
            }

            List<String[]> data = new ArrayList<>();
            SearchResult<CompanyPayroll> searchResult = companyPayrollService.selectAllCompanyPayrolls();

            if (searchResult.getCount() > 0) {
                dataTablesSearchResult.setRecordsFiltered(searchResult.getCount());
                dataTablesSearchResult.setRecordsTotal(searchResult.getCount());

                int i = offset;
                String labelEdit = getMessage("button.edit");
                String labelDetail = getMessage("button.detail");
                for (CompanyPayroll result : searchResult.getList()) {
                    data.add(new String[]{
                            ""+ (++i),
                            result.getCompanyId().getName(),
                            dateUtil.formatDate(result.getCreatedDate(), StringConstant.DATE_FORMAT_A),
                            dateUtil.formatDate(result.getCheckedDate(), StringConstant.DATE_FORMAT_A),
                            result.getStatus().equalsIgnoreCase("disetujui") ? "<div class=\"label label-success\" >" + result.getStatus() + "</div>" : "<div class=\"label label-danger\" >" + result.getStatus() + "</div>",
                            "<div class=\"btn-group btn-group-xs\" role=\"group\">" +
                                    "<a href=\"" + getPathURL(PATH_DETAIL + "/" + result.getId()) + "\" class=\"btn btn-default\"><i class=\"fa fa-edit\"></i> <span>" + labelDetail + "</span></a>" +
                            "<a href=\"" + getPathURL(PATH_EDIT + "/" + result.getId()) + "\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i> <span>" + labelEdit + "</span></a>" +
                                    "</div>"
                    });
                }
            }
            dataTablesSearchResult.setData(data);

            writeJsonResponse(response, dataTablesSearchResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /************************* DETAIL PAGE *********************/
    public static final String PAGE_DETAIL = PAGE_DIRECTORY + "detail";
    public static final String PATH_DETAIL = PATH_DIRECTORY + "detail";
    private static final String REQUEST_MAPPING_DETAIL = PATH_DETAIL + "/{id}";


    @GetMapping(value = REQUEST_MAPPING_DETAIL)
    public String doShowDetailCompanyPayroll(@PathVariable("id") String id, final Model model, final HttpServletRequest request) {
        CompanyPayroll companyPayroll = companyPayrollService.selectOneCompanyPayroll(id);
//        SearchResult<CompanyPayrollDetail> listDetail= companyPayrollService.selectAllCompanyPayrollDetailsByCompanyPayrollId(id);

        if (companyPayroll == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.company.payroll.edit.invalid");
        }

        try {
            String labelMonth = dateUtil.getMonthName(new Locale("id"), companyPayroll.getPeriodeMonth());
//            model.addAttribute("periodeMonth", labelMonth);
            companyPayroll.setPeriodeMonthStr(labelMonth);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }


        model.addAttribute("data", companyPayroll);

//        model.addAttribute("listDetail", listDetail);


        return getPageContent(PAGE_DETAIL);
    }

    public static final String PATH_AJAX_SEARCH_DETAIL_LIST = PATH_DIRECTORY + "search-detail";
    public static final String PATH_AJAX_DETAIL_LIST = PAGE_DIRECTORY + "search-detail-list";

    private static final String REQUEST_MAPPING_AJAX_SEARCH_DETAIL_LIST = PATH_AJAX_SEARCH_DETAIL_LIST + "*";

    @RequestMapping(value = REQUEST_MAPPING_AJAX_SEARCH_DETAIL_LIST, method = {RequestMethod.POST})
    public void doSearchDataTablesDetailList( final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            String id = request.getParameter("idComp");

            DataTablesSearchResult dataTablesSearchResult = new DataTablesSearchResult();
            try {
                dataTablesSearchResult.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesSearchResult.setDraw(0);
            }

            int offset = 0;
            int limit = 50;
            try {
                offset = Math.abs(Integer.parseInt(request.getParameter("start")));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Math.abs(Integer.parseInt(request.getParameter("length")));
            } catch (NumberFormatException e) {
            }

            List<String[]> data = new ArrayList<>();
            SearchResult<CompanyPayrollDetail> searchResult = companyPayrollService.selectAllCompanyPayrollDetailsByCompanyPayrollId(id);

            if (searchResult.getCount() > 0) {
                dataTablesSearchResult.setRecordsFiltered(searchResult.getCount());
                dataTablesSearchResult.setRecordsTotal(searchResult.getCount());

                int i = offset;
                for (CompanyPayrollDetail result : searchResult.getList()) {
                    data.add(new String[]{
                            ""+(++i),
                            result.getEmpId(),
                            result.getEmpName(),
                            result.getEmpDept(),
                            result.getDestAccount(),
                            result.getAmount().toString(),
                            dateUtil.formatDate(result.getPaymentDate(), StringConstant.DATE_FORMAT_A)
                    });
                }
            }
            dataTablesSearchResult.setData(data);

            writeJsonResponse(response, dataTablesSearchResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /************************* EDIT PAGE *********************/
    public static final String PAGE_EDIT = PAGE_DIRECTORY + "edit";
    public static final String PATH_EDIT = PATH_DIRECTORY + "edit";
    private static final String REQUEST_MAPPING_EDIT = PATH_EDIT + "/{id}";
    public static final String COMMAND_EDIT = "redirect:" + PATH_EDIT;
    public static final String COMMAND_LIST = "redirect:" + PATH_LIST;


    @GetMapping(value = REQUEST_MAPPING_EDIT)
    public String doShowEditCompanyPayroll(@PathVariable("id") String id, final Model model, final HttpServletRequest request) {
        CompanyPayroll companyPayroll = companyPayrollService.selectOneCompanyPayroll(id);

        if (companyPayroll == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.company.payroll.edit.invalid");
        }

        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        try {
            String labelMonth = dateUtil.getMonthName(new Locale("id"), companyPayroll.getPeriodeMonth());
//            model.addAttribute("periodeMonth", labelMonth);
            companyPayroll.setPeriodeMonthStr(labelMonth);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        model.addAttribute("data", companyPayroll);

        return getPageContent(PAGE_EDIT);
    }

    @PostMapping(value = REQUEST_MAPPING_EDIT)
    public String doEditCompanyPayroll(@PathVariable("id") String id, @ModelAttribute("data") final CompanyPayroll companyPayroll,
                                       final BindingResult errors, final Model model, final HttpServletRequest request) {

        CompanyPayroll originalData = companyPayrollService.selectOneCompanyPayroll(id);

        if (originalData == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.company.payroll.edit.invalid");
        }

//        formValidationUtil.validatingMandatoryInput(errors, "status", companyPayroll.getStatus(), getMessage("label.status"));
        formValidationUtil.validatingOptionalInput(errors, "reason", companyPayroll.getReason(), getMessage("label.reason"), FieldConstant.MAX_LENGTH_REASON);
//        formValidationUtil.validatingInput(errors, "taCompanyId.name", companyPayroll.getTaCompanyId().getName(), getMessage("label.taCompany.name"), FieldConstant.MAX_LENGTH_NAME);
//        formValidationUtil.validatingInput(errors, "companyId.name", companyPayroll.getCompanyId().getName(), getMessage("label.company.name"), FieldConstant.MAX_LENGTH_NAME);
//        formValidationUtil.validatingMandatoryInput(errors, "periodeYear", companyPayroll.getPeriodeYear().toString(), getMessage("label.periode.year"));
//        formValidationUtil.validatingMandatoryInput(errors, "periodeMonth", companyPayroll.getPeriodeMonth().toString(), getMessage("label.periode.month"));
//        formValidationUtil.validatingMandatoryInput(errors, "createdBy", companyPayroll.getCreatedBy().getName(), getMessage("label.created.by"));
//        formValidationUtil.validatingMandatoryInput(errors, "createdDate", companyPayroll.getCreatedDate().toString(), getMessage("label.created.date"));

        if (!errors.hasErrors()) {
            try {
                originalData.setStatus(companyPayroll.getStatus());
                originalData.setReason(companyPayroll.getReason());
                companyPayrollService.updateStatusCompanyPayroll(originalData);
                String okMessage = getMessage("form.ok.trx.company.payroll.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", originalData.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_EDIT);
            }
        } else return getPageContent(PAGE_EDIT) + "?errorMessage=" + getMessage("message.error.data");

    }

    /************************* UPLOAD PAGE *********************/
    @RequestMapping(value = {REQUEST_MAPPING_PAYROLL}, method = {RequestMethod.GET})
    public String doShowPage(@RequestParam(value = "error", required = false) String error,
                             final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        if (okMessage != null) {
            model.addAttribute("okMessage", okMessage);
        }

        model.addAttribute("data", new CompanyPayroll());

        SearchResult<Company> listCompany = companyService.selectAllActiveCompaniesByLoggedInUser();
        model.addAttribute("companies", listCompany.getList());

        return getPageContent(PAGE_PAYROLL_UPLOAD);
    }

    @RequestMapping(value = {REQUEST_MAPPING_PAYROLL}, method = {RequestMethod.POST})
    public String doUploadFile(@RequestParam(value = "error", required = false) String error,
                               @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
                               @ModelAttribute("data") CompanyPayroll companyPayroll,
                               final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        if (okMessage != null) {
            model.addAttribute("okMessage", okMessage);
        }

        companyPayroll.setStatus("Ditolak");
        try {
            companyPayrollService.insertCompanyPayroll(companyPayroll, uploadedFile);
            model.addAttribute("okMessage", getMessage("message.ok.data"));
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", getMessage("message.error.data"));
        }

        return getPageContent(PAGE_LIST);
    }


    /********************************************** - LIST SECTION - **********************************************/


    public static final String REQUEST_MAPPING_LIST = PATH_LIST + "*";


    @RequestMapping(value = {REQUEST_MAPPING_LIST}, method = {RequestMethod.GET, RequestMethod.POST})
    public String doShowListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");
        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("companyPayrolls", companyPayrollService.selectAllCompanyPayrolls().getList());
        model.addAttribute("activeMenu", "input");

        return getPageContent(PAGE_LIST);
    }


    /********************************************** - GLOBAL MODEL ATTRIBUTE SECTION - **********************************************/
    @ModelAttribute
    public void addModelAttribute(Model model) {
        model.addAttribute("title", getMessage("page.title.trx.company.payroll"));
    }

}
