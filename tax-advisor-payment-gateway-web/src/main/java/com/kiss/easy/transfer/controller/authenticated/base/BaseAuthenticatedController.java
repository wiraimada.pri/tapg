package com.kiss.easy.transfer.controller.authenticated.base;

import com.kiss.web.thymeleaf.controller.BaseController;

public abstract class BaseAuthenticatedController extends BaseController {
	protected static final String PAGE_BASE = "authenticated/";

	public static final String PATH_BASE = "/admin/";
}
