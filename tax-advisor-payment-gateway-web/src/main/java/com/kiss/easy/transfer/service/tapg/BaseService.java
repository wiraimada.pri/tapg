package com.kiss.easy.transfer.service.tapg;

import com.kiss.easy.transfer.entity.BaseEntity;
import com.kiss.easy.transfer.entity.MasterEntity;
import com.kiss.easy.transfer.entity.TransactionEntity;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.UUID;

/**
 * Created by NEVIIM - DIX on 16/01/2019.
 */
public abstract class BaseService {
	@Autowired
	private DateUtil dateUtil;

	protected User getLoggedUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	protected void prepareInsert(BaseEntity entity) {
		entity.setId(UUID.randomUUID().toString());
	}

	protected void prepareInsert(MasterEntity entity) {
		entity.setId(UUID.randomUUID().toString());
		entity.setCreatedBy(getLoggedUser());
		entity.setCreatedDate(dateUtil.now());
	}

	protected void prepareInsert(TransactionEntity entity) {
		entity.setId(UUID.randomUUID().toString());
		entity.setCreatedBy(getLoggedUser());
		entity.setCreatedDate(dateUtil.now());
	}

	protected void prepareUpdate(MasterEntity entity) {
		entity.setRowVersion(entity.getRowVersion() + 0.1);
		entity.setLastUpdatedBy(getLoggedUser());
		entity.setLastUpdatedDate(dateUtil.now());
	}
}
