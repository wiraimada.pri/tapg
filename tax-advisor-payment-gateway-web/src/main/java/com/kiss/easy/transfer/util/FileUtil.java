package com.kiss.easy.transfer.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * Created by NEVIIM - DIX on 16/01/2019.
 */
@Component
public class FileUtil {
    public File saveMultipartFileToFile(MultipartFile uploadedFile, String path, String fileName) throws IOException {
        String fileExtension = uploadedFile.getOriginalFilename().split("\\.")
                [uploadedFile.getOriginalFilename().split("\\.").length - 1]
                .toLowerCase();

        File file = new File(path + fileName + "." + fileExtension);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        byte[] buff = new byte[5120];
        BufferedInputStream bis = new BufferedInputStream(uploadedFile.getInputStream());
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        while (bis.available() > 0) {
            bis.read(buff);
            bos.write(buff);
        }
        bos.close();
        bis.close();
        return file;
    }
}
