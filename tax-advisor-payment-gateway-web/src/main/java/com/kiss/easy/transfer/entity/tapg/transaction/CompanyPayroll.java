package com.kiss.easy.transfer.entity.tapg.transaction;

import com.kiss.easy.transfer.entity.BaseEntity;
import com.kiss.easy.transfer.entity.TransactionEntity;
import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.sql.Timestamp;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.trx_company_payroll")
public class CompanyPayroll extends TransactionEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "status")
    private String status;
    @Column(name = "reason")
    private String reason;
    @Column(name = "m_ta_company_id", referenceField = "id")
    private TaCompany taCompanyId;
    @Column(name = "m_company_id", referenceField = "id")
    private Company companyId;
    @Column(name = "periode_year")
    private Integer periodeYear;
    @Column(name = "periode_month")
    private Integer periodeMonth;
//    @Column(name = "created_by", referenceField = "id")
//    private User createdBy;
//    @Column(name = "created_date")
//    private Timestamp createdDate;
    @Column(name = "checked_by", referenceField = "id")
    private User checkedBy;
    @Column(name = "checked_date")
    private Timestamp checkedDate;
//    @Column(name = "row_version")
//    private double rowVersion = 0.9;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/

    private String checkedDateStr;
    private String createdDateStr;
    private String periodeMonthStr;

    public String getCheckedDateStr() {
        return checkedDateStr;
    }

    public void setCheckedDateStr(String checkedDateStr) {
        this.checkedDateStr = checkedDateStr;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getPeriodeMonthStr() {
        return periodeMonthStr;
    }

    public void setPeriodeMonthStr(String periodeMonthStr) {
        this.periodeMonthStr = periodeMonthStr;
    }
    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public TaCompany getTaCompanyId() {
        return taCompanyId;
    }

    public void setTaCompanyId(TaCompany taCompanyId) {
        this.taCompanyId = taCompanyId;
    }

    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }

    public Integer getPeriodeYear() {
        return periodeYear;
    }

    public void setPeriodeYear(Integer periodeYear) {
        this.periodeYear = periodeYear;
    }

    public Integer getPeriodeMonth() {
        return periodeMonth;
    }

    public void setPeriodeMonth(Integer periodeMonth) {
        this.periodeMonth = periodeMonth;
    }

    public void setCheckedBy(User checkedBy) {
        this.checkedBy = checkedBy;
    }

    public User getCheckedBy() {
        return checkedBy;
    }

    public Timestamp getCheckedDate() {
        return checkedDate;
    }

    public void setCheckedDate(Timestamp checkedDate) {
        this.checkedDate = checkedDate;
    }



    //    public double getRowVersion() {
//        return this.rowVersion;
//    }
//
//    public void setRowVersion(double rowVersion) {this.rowVersion = rowVersion;}

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
}
