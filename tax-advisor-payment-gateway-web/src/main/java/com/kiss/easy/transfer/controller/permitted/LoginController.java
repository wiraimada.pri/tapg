package com.kiss.easy.transfer.controller.permitted;

import com.kiss.web.thymeleaf.annotation.Decorator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 4/17/15.
 */
@Controller
public class LoginController extends BasePermittedController {
    private static final String LOGIN_ERROR_ACCOUNT_DISABLED = "acc-dis";
    private static final String LOGIN_ERROR_ACCOUNT_EXPIRED = "acc-exp";
    private static final String LOGIN_ERROR_ACCOUNT_INVALID = "invalid";
    private static final String LOGIN_ERROR_ACCOUNT_LOCKED = "acc-locked";
    private static final String LOGIN_ERROR_CREDENTIAL_EXPIRED = "cred-exp";
    private static final String LOGIN_ERROR_NO_AUTHORITY = "no-auth";
    private static final String LOGIN_ERROR_SESSION_EXPIRED = "ses-exp";

    private static final String PAGE_LOGIN = DIRECTORY_PAGE + "login";
    public static final String PATH_LOGIN = "/login";

    private static final String PATH_LOGIN_ERROR = PATH_LOGIN + "?error=";
    public static final String PATH_LOGIN_ERROR_ACCOUNT_DISABLED = PATH_LOGIN_ERROR + LOGIN_ERROR_ACCOUNT_DISABLED;
    public static final String PATH_LOGIN_ERROR_ACCOUNT_EXPIRED = PATH_LOGIN_ERROR + LOGIN_ERROR_ACCOUNT_EXPIRED;
    public static final String PATH_LOGIN_ERROR_ACCOUNT_INVALID = PATH_LOGIN_ERROR + LOGIN_ERROR_ACCOUNT_INVALID;
    public static final String PATH_LOGIN_ERROR_ACCOUNT_LOCKED = PATH_LOGIN_ERROR + LOGIN_ERROR_ACCOUNT_LOCKED;
    public static final String PATH_LOGIN_ERROR_CREDENTIAL_EXPIRED = PATH_LOGIN_ERROR + LOGIN_ERROR_CREDENTIAL_EXPIRED;
    public static final String PATH_LOGIN_ERROR_NO_AUTHORITY = PATH_LOGIN_ERROR + LOGIN_ERROR_NO_AUTHORITY;
    public static final String PATH_LOGIN_ERROR_SESSION_EXPIRED = PATH_LOGIN_ERROR + LOGIN_ERROR_SESSION_EXPIRED;

    public static final String PATH_LOGOUT = "/logout";
    public static final String PATH_LOGOUT_SUCCESS = PATH_LOGIN + "?logout=ok";

    public static final String COMMAND_LOGIN = "redirect:" + PATH_LOGIN;
    public static final String COMMAND_LOGIN_ERROR = "redirect:" + PATH_LOGIN_ERROR;
    public static final String COMMAND_LOGOUT = "redirect:" + PATH_LOGOUT;
    public static final String COMMAND_LOGOUT_SUCCESS = "redirect:" + PATH_LOGOUT_SUCCESS;

    public static final String REQUEST_MAPPING_LOGIN = PATH_LOGIN + "*";
    public static final String REQUEST_MAPPING_LOGOUT = PATH_LOGOUT + "*";

    @Decorator("login")
    @RequestMapping(value = { "/", REQUEST_MAPPING_LOGIN }, method = { RequestMethod.GET })
    public String doShowPage(@RequestParam(value = "error", required = false) String error,
                             @RequestParam(value = "logout", required = false) String logout,
                             final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        if(okMessage != null){
            model.addAttribute("okMessage", okMessage);
        }

        if (request.getUserPrincipal() != null) {
			doInvalidateUser(request, response);
			StringBuffer sbResponse = new StringBuffer();
			if (error != null && !error.equalsIgnoreCase("")) {
				sbResponse.append("&error=");
				sbResponse.append(error);
			}
			if (logout != null && !logout.equalsIgnoreCase("")) {
				sbResponse.append("&logout=");
				sbResponse.append(logout);
			}
			if (sbResponse.length() > 0) {
				sbResponse.deleteCharAt(0);
				sbResponse.insert(0, "?");
			}
			sbResponse.insert(0, COMMAND_LOGIN);
			return sbResponse.toString();
		}

        if (error != null) {
            if (error.equalsIgnoreCase(LOGIN_ERROR_ACCOUNT_DISABLED)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.account.disabled"));
            } else if (error.equalsIgnoreCase(LOGIN_ERROR_ACCOUNT_EXPIRED)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.account.expired"));
            } else if (error.equalsIgnoreCase(LOGIN_ERROR_ACCOUNT_LOCKED)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.account.locked"));
            } else if (error.equalsIgnoreCase(LOGIN_ERROR_CREDENTIAL_EXPIRED)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.credential.expired"));
            } else if (error.equalsIgnoreCase(LOGIN_ERROR_SESSION_EXPIRED)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.session.expired"));
            } else if (error.equalsIgnoreCase(LOGIN_ERROR_NO_AUTHORITY)) {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in.no.authority"));
            } else {
                model.addAttribute("errorMessage", getMessage("form.error.sign.in"));
            }
        }

        if (logout != null && !logout.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", getMessage("form.ok.sign.out"));
        }

        model.addAttribute("title", getMessage("page.title.login"));
        return getPageContent(PAGE_LOGIN);
    }

    /********************************************** - LOGOUT SECTION - **********************************************/
    @RequestMapping(value = { REQUEST_MAPPING_LOGOUT }, method = { RequestMethod.GET, RequestMethod.POST })
    public String doLogout(final HttpServletRequest request, final HttpServletResponse response) {
        doInvalidateUser(request, response);

        return COMMAND_LOGOUT_SUCCESS;
    }

    /********************************************** - OTHER SECTION - **********************************************/
    private void doInvalidateUser(final HttpServletRequest request, final HttpServletResponse response) {
        new SecurityContextLogoutHandler().logout(request, response, SecurityContextHolder.getContext().getAuthentication());
        if (request.getSession(false) != null) {
            request.getSession(false).invalidate();
        }
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if ("JSESSIONID".equalsIgnoreCase(cookie.getName())) {
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setDomain(request.getServerName());
                cookie.setPath(request.getServletContext().getContextPath() + "/");
                cookie.setSecure(request.isSecure());
                response.addCookie(cookie);
                break;
            }
        }

        request.getSession(true);
    }
}