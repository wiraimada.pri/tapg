package com.kiss.easy.transfer.entity.tapg.master;

import com.kiss.easy.transfer.entity.MasterEntity;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.m_role")
public class Role extends MasterEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "name")
    private String name;
    @Column(name = "m_ta_company_id", referenceField = "id")
    private TaCompany taCompany;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/
    private List<RoleMenu> roleMenus;
    private List<String> listRoleMenuId;

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaCompany getTaCompany() {return taCompany;}

    public void setTaCompany(TaCompany taCompany) {
        this.taCompany = taCompany;
    }
    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/
    public List<RoleMenu> getRoleMenus() {return roleMenus;}

    public void setRoleMenus(List<RoleMenu> roleMenus) {
        this.roleMenus = roleMenus;
        List<String> menuIds = new ArrayList<>();
        for (RoleMenu roleMenu : roleMenus) {
            menuIds.add(roleMenu.getMenu().getId());
        }
        setListRoleMenuId(menuIds);
    }

    public List<String> getListRoleMenuId() {
        return listRoleMenuId;
    }

    public void setListRoleMenuId(List<String> listRoleMenuId) {
        this.listRoleMenuId = listRoleMenuId;
    }

    /***************************** - OTHER METHOD SECTION - ****************************/
}
