package com.kiss.easy.transfer.entity.tapg.master;

import com.kiss.easy.transfer.entity.UserEntity;
import com.kiss.easy.transfer.entity.shared.Authority;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.easy.transfer.entity.tapg.statics.UserType;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.util.*;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.m_user")
public class User extends UserEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "email")
    private String email;
	@Column(name = "s_user_type_name", referenceField = "name")
	private UserType userType;
    @Column(name = "m_ta_company_id", referenceField = "id")
    private TaCompany taCompany;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/
    private List<Role> roleList;
    private List<RoleMenu> roleMenuList;
    private List<UserRole> userRoles;
    private List<String> roleIds;
    private Map<String, List<Menu>> roleMenuMap;
	private List<Menu> menuList;
    private String newPassword;
    private String confirmPassword;
    private String currentPassword;
    private String oldUsername;

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public TaCompany getTaCompany() {
        return taCompany;
    }

    public void setTaCompany(TaCompany taCompany) {
        this.taCompany = taCompany;
    }

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public void setRoleMenuList(List<RoleMenu> roleMenuList) {
		this.roleMenuList = roleMenuList;
		this.roleMenuMap = new HashMap<>();
		this.menuList = new ArrayList<>();

		if (this.roleMenuList != null && this.roleMenuList.size() > 0) {
			Map<String, Menu> menuMap = new HashMap<>();
			List<Menu> childMenu = new ArrayList<>();
			for (RoleMenu roleMenu : this.roleMenuList) {
				List<Menu> menuListPerRole;
				if (this.roleMenuMap.containsKey(roleMenu.getId())) {
					menuListPerRole = this.roleMenuMap.get(roleMenu.getId());
				} else {
					menuListPerRole = new ArrayList<>();
					this.roleMenuMap.put(roleMenu.getId(), menuListPerRole);
				}
				menuListPerRole.add(roleMenu.getMenu());
				if (!menuMap.containsKey(roleMenu.getMenu().getId())) {
					menuMap.put(roleMenu.getMenu().getId(), roleMenu.getMenu());
					if (roleMenu.getMenu().getParentMenu() == null) {
						this.menuList.add(roleMenu.getMenu());
					} else {
						childMenu.add(roleMenu.getMenu());
					}
				}
			}
			for (Menu menu : childMenu) {
				Menu parentMenu = menuMap.get(menu.getParentMenu().getId());
				parentMenu.addChildMenu(menu);
			}
		}
	}

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
        List<String> roleIds = new ArrayList<>();
        for (UserRole userRole : userRoles) {
            roleIds.add(userRole.getRole().getId());
        }
        setRoleIds(roleIds);
    }

    public List<Menu> getMenuList() {
		return menuList;
	}

	public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getOldUsername() {
        return oldUsername;
    }

    public void setOldUsername(String oldUsername) {
        this.oldUsername = oldUsername;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    /***************************** - OTHER METHOD SECTION - ****************************/
	@Override
	protected Authority getAuthority() {
		return userType;
	}

	public List<Menu> getMenuListPerRole(Role role) {
		return this.getMenuListPerRole(role.getId());
	}

	public List<Menu> getMenuListPerRole(String roleId) {
		return this.roleMenuMap.get(roleId);
	}
}