package com.kiss.easy.transfer.enumeration;

import com.kiss.easy.transfer.entity.tapg.statics.UserType;

public enum UserTypeEnum {
	USER,
	SYSTEM;

	public UserType value() {
		return new UserType(name());
	}
}
