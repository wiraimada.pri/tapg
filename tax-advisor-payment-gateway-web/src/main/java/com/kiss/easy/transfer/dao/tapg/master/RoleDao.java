package com.kiss.easy.transfer.dao.tapg.master;

import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.RoleMenu;
import com.kiss.mybatis.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.UpdateQuery;

import java.util.List;

/**
 * Created by dixio69 on 21/01/2019 in the 'easy-transfer' Project.
 */
@Component
public class RoleDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public long countByTaCompanyId(String taCompanyId){
        return queryBuilder
                .count()
                .from(QueryBuilder.CountQuery.from(Role.class, "t0"))
                .where(QueryBuilder.CountQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId))
                .execute();
    }

    public int deleteRole(String id) {
        return queryBuilder
                .delete(Role.class)
                .where(DeleteQuery.condition("id", EquationTypeEnum.EQUALS, id))
                .execute();
    }

    public int insertRole(Role role) {
        return queryBuilder
                .insert(role.getClass())
                .setParameter(role)
                .execute();
    }

    public List<Role> selectAllRole(){
        return queryBuilder
                .select()
                .from(SelectQuery.from(Role.class, "t0"))
                .asList(Role.class);
    }

    public List<Role> selectAllByTaCompanyId(String taCompanyId){
        return queryBuilder
                .select()
                .from(SelectQuery.from(Role.class, "t0"))
                .where(SelectQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS_IGNORECASE, taCompanyId))
                .asList(Role.class);
    }

//    public List<Role> selectAllRoleHaveRoleMenu(){
//        return queryBuilder.select("distinct t1.*")
//                .from(SelectQuery.from(RoleMenu.class, "t0"))
//                .join(
//                        SelectQuery.join(QueryBuilder.JoinTypeEnum.JOIN, Role.class, "t1",
//                                SelectQuery.condition("t0", "role", EquationTypeEnum.EQUALS, "t1", "id"), null)
//                )
//                .orderBy(SelectQuery.orderByAscending("t1", "name"))
//                .asList(Role.class);
//    }

    public Role selectOneById(String id){
        return queryBuilder
                .select()
                .from(SelectQuery.from(Role.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(Role.class);
    }

    public int updateRole(Role role) {
        return queryBuilder
                .update(role.getClass())
                .setParameter(role)
                .where(UpdateQuery.condition("id", EquationTypeEnum.EQUALS, role.getId()))
                .execute();
    }
}
