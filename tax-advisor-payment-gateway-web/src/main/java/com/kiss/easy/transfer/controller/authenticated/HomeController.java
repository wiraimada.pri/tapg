package com.kiss.easy.transfer.controller.authenticated;

import com.kiss.easy.transfer.controller.authenticated.base.BaseAuthenticatedController;
import com.kiss.easy.transfer.enumeration.MenuEnum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 1/14/19.
 */
@Controller
public class HomeController extends BaseAuthenticatedController {
	private static final String PAGE_HOME = PAGE_BASE + "home";

	public static final String PATH_HOME = PATH_BASE + "home";

	@GetMapping({PATH_HOME + "*"})
	public String doShowPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		model.addAttribute("activeMenu", MenuEnum.HOME.id());
		model.addAttribute("title", getMessage("page.title.home"));

		return getPageContent(PAGE_HOME);
	}
}
