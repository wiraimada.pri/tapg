package com.kiss.easy.transfer.dao.tapg.statics;

import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by dixio69 on 21/01/2019 in the 'easy-transfer' Project.
 */
@Component
public class MenuDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public List<Menu> selectAll(){
        return queryBuilder
                .select()
                .from(SelectQuery.from(Menu.class, "t0"))
                .asList(Menu.class);
    }

    public Menu selectOneById(String id){
        return queryBuilder
                .select()
                .from(SelectQuery.from(Menu.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(Menu.class);
    }
}
