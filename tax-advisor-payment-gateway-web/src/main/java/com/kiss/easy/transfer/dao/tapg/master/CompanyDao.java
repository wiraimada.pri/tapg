package com.kiss.easy.transfer.dao.tapg.master;

import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.CountQuery;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.UpdateQuery;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 11/01/2019.
 */
@Component
public class CompanyDao {
    @Autowired
    private QueryBuilder queryBuilder;


    /******************* ~ METHODS ~ ************************/
    public long count() {
        return queryBuilder
                .count()
                .from(CountQuery.from(Company.class, "t0"))
                .execute();
    }

    public long countActiveCompany() {
        return queryBuilder
                .count()
                .from(CountQuery.from(Company.class, "t0"))
                .where(CountQuery.condition("t0", "active", EquationTypeEnum.EQUALS, true))
                .execute();
    }

    public long countActiveCompanyByTaCompanyId(String taCompanyId) {
        return queryBuilder
                .count()
                .from(CountQuery.from(Company.class, "t0"))
                .where(CountQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId)
                        .and((CountQuery.condition("t0", "active", EquationTypeEnum.EQUALS, true)))
                )
                .execute();
    }

    public long countCompanyByTaCompanyId(String taCompanyId) {
        return queryBuilder
                .count()
                .from(CountQuery.from(Company.class, "t0"))
                .where(CountQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId))
                .execute();
    }

    public int deleteCompany(String id) {
        return queryBuilder
                .delete(Company.class)
                .where(DeleteQuery.condition("id", EquationTypeEnum.EQUALS, id))
                .execute();
    }

    public int insertCompany(Company company) {
        return queryBuilder
                .insert(company.getClass())
                .setParameter(company)
                .execute();
    }

    public List<Company> selectAll() {
        return queryBuilder
                .select()
                .from(SelectQuery.from(Company.class, "t0"))
                .asList(Company.class);
    }

    public List<Company> selectAllActiveCompany() {
        return queryBuilder
                .select()
                .from(SelectQuery.from(Company.class, "t0"))
                .where(SelectQuery.condition("t0", "active", EquationTypeEnum.EQUALS, true))
                .asList(Company.class);
    }

    public List<Company> selectAllActiveCompanyByTaCompanyId(String taCompanyId) {
        return queryBuilder
                .select()
                .from(SelectQuery.from(Company.class, "t0"))
                .where(SelectQuery.condition("t0", "active", EquationTypeEnum.EQUALS, true)
                        .and((SelectQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId)))
                )
                .asList(Company.class);
    }

    public List<Company> selectAllCompanyByTaCompanyId(String taCompanyId) {
        return queryBuilder
                .select()
                .from(SelectQuery.from(Company.class, "t0"))
                .where(SelectQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId))
                .asList(Company.class);
    }

    public Company selectOneById(String id) {
        return queryBuilder
                .select()
                .from(SelectQuery.from(Company.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(Company.class);
    }

    public int updateCompany(Company company) {
        return queryBuilder
                .update(company.getClass())
                .setParameter(company)
                .where(UpdateQuery.condition("id", EquationTypeEnum.EQUALS, company.getId()))
                .execute();
    }


}
