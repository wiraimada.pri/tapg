package com.kiss.easy.transfer.controller.authenticated.base;

/**
 * Created by dixio69 on 18/01/2019 in the 'easy-transfer' Project.
 */
public abstract class BaseManagementController extends BaseAuthenticatedController {
    protected static final String PAGE_BASE = BaseAuthenticatedController.PAGE_BASE + "management/";

    public static final String PATH_BASE = BaseAuthenticatedController.PATH_BASE + "management/";
}
