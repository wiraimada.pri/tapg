package com.kiss.easy.transfer.entity.tapg.statics;

import com.kiss.easy.transfer.entity.shared.Authority;
import com.kiss.mybatis.annotation.Entity;

@Entity(name = "tapg.s_user_type")
public class UserType extends Authority {
	/***************************** - FIELD SECTION - ****************************/

	/***************************** - TRANSIENT FIELD SECTION - ****************************/

	/***************************** - CONSTRUCTOR SECTION - ****************************/

	public UserType() {
	}

	public UserType(String name) {
		super(name);
	}

	/***************************** - GETTER SETTER METHOD SECTION - ****************************/

	/***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

	/***************************** - OTHER METHOD SECTION - ****************************/
}