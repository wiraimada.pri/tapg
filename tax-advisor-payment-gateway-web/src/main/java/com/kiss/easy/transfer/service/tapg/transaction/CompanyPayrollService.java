package com.kiss.easy.transfer.service.tapg.transaction;

import com.kiss.easy.transfer.constant.StringConstant;
import com.kiss.easy.transfer.dao.tapg.master.UserDao;
import com.kiss.easy.transfer.dao.tapg.transaction.CompanyPayrollDao;
import com.kiss.easy.transfer.dao.tapg.transaction.CompanyPayrollDetailDao;
import com.kiss.easy.transfer.dao.tapg_admin.master.TaCompanyDao;
import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayroll;
import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayrollDetail;
import com.kiss.easy.transfer.exception.TapgException;
import com.kiss.easy.transfer.service.tapg.BaseService;
import com.kiss.easy.transfer.service.tapg.master.CompanyService;
import com.kiss.easy.transfer.util.FileUtil;
import com.kiss.easy.transfer.util.XlsxReaderUtil;
import com.kiss.pojo.SearchResult;
import com.kiss.util.DateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Service
public class CompanyPayrollService extends BaseService {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private CompanyPayrollDao companyPayrollDao;
    @Autowired
    private CompanyPayrollDetailDao companyPayrollDetailDao;
    @Autowired
    private TaCompanyDao taCompanyDao;
    @Autowired
    private UserDao userDao;

    @Autowired
    private DateUtil dateUtil;
    @Autowired
    private FileUtil fileUtil;
    @Autowired
    private XlsxReaderUtil xlsxReaderUtil;
    /************************ FILE DIRECTORY *********************/
    @Value("${upload.file.path:}")
    private String uploadFilePath;

    /************************ METHODS ****************************/
    public long countCompanyPayrollDetailByCompanyPayrollId(String companyPayrollId) {
        return companyPayrollDetailDao.countByCompanyPayrollId(companyPayrollId);
    }

    @Transactional
    public void insertCompanyPayroll(CompanyPayroll companyPayroll, MultipartFile uploadedFile) throws IOException, InvalidFormatException {
        super.prepareInsert(companyPayroll);

        companyPayroll.setPeriodeMonth(dateUtil.getMonth());
        companyPayroll.setPeriodeYear(dateUtil.getYear());
        companyPayroll.setTaCompanyId(getLoggedUser().getTaCompany());
        companyPayrollDao.insertCompanyPayroll(companyPayroll);
        File file = fileUtil.saveMultipartFileToFile(uploadedFile, uploadFilePath, companyPayroll.getId());
        readUploadedFileAsCompanyPayrollDetails(file, companyPayroll);
    }

    public void insertCompanyPayrollDetail(CompanyPayrollDetail companyPayrollDetail) {
        super.prepareInsert(companyPayrollDetail);
        companyPayrollDetailDao.insertCompanyPayroll(companyPayrollDetail);
    }

    public List<CompanyPayroll> selectAll(){
        List<CompanyPayroll> companyPayrolls = companyPayrollDao.selectAll();
        for (CompanyPayroll cp : companyPayrolls) {
            cp.setCompanyId(companyService.selectOneCompanyById(cp.getCompanyId().getId()));
        }
        return companyPayrolls;
    }

    public SearchResult<CompanyPayroll> selectAllCompanyPayrolls() {
        SearchResult<CompanyPayroll> searchResult = new SearchResult<>();
        searchResult.setCount((int) companyPayrollDao.count());
        if (searchResult.getCount() > 0) {
            searchResult.setList(selectAll());
        }
        return searchResult;
    }

    public SearchResult<CompanyPayroll> selectAllCompanyPayrollsByCompanyId(String companyId) {
        SearchResult<CompanyPayroll> searchResult = new SearchResult<>();
        searchResult.setCount((int) companyPayrollDao.countByCompanyId(companyId));
        if (searchResult.getCount() > 0) {
            searchResult.setList(companyPayrollDao.selectAllByCompanyId(companyId));
        }
        return searchResult;
    }

    public SearchResult<CompanyPayrollDetail> selectAllCompanyPayrollDetailsByCompanyPayrollId(String companyPayrollId) {
        SearchResult<CompanyPayrollDetail> searchResult = new SearchResult<>();
        searchResult.setCount((int) companyPayrollDetailDao.countByCompanyPayrollId(companyPayrollId));
        if (searchResult.getCount() > 0) {
            searchResult.setList(companyPayrollDetailDao.selectAllByCompanyPayrollId(companyPayrollId));
        }
        return searchResult;
    }

    public CompanyPayroll selectOneCompanyPayroll(String id) {
        CompanyPayroll companyPayroll = companyPayrollDao.selectOneById(id);
        companyPayroll.setTaCompanyId(taCompanyDao.selectOne(companyPayroll.getTaCompanyId().getId()));
        Company company = companyService.selectOneCompanyById(companyPayroll.getCompanyId().getId());
        companyPayroll.setCompanyId(company);

        companyPayroll.setCreatedBy(userDao.selectOneById(companyPayroll.getCreatedBy().getId()));

//        User checkedBy = userDao.selectOneById(companyPayroll.getCheckedBy().getId());
        if(companyPayroll.getCheckedBy() != null)
            companyPayroll.setCheckedBy(userDao.selectOneById(companyPayroll.getCheckedBy().getId()));
        else
            companyPayroll.setCheckedBy(null);
        companyPayroll.setCreatedDateStr(dateUtil.formatDate(companyPayroll.getCreatedDate(), StringConstant.DATE_FORMAT_A));
        companyPayroll.setCheckedDateStr(dateUtil.formatDate(companyPayroll.getCheckedDate(), StringConstant.DATE_FORMAT_A));
        return companyPayroll;
    }

    public CompanyPayrollDetail selectOneCompanyPayrollDetail(String id) {
        return companyPayrollDetailDao.selectOneById(id);
    }

    @Transactional
    public void updateStatusCompanyPayroll(CompanyPayroll companyPayroll) {
        companyPayroll.setCheckedBy(getLoggedUser());
        companyPayroll.setCheckedDate(dateUtil.now());
        companyPayrollDao.updateCompanyPayroll(companyPayroll);
    }

    /******************** OTHER METHODS *******************/
    private List<CompanyPayrollDetail> readUploadedFileAsCompanyPayrollDetails(File file, CompanyPayroll companyPayroll) throws IOException, InvalidFormatException {
        List<CompanyPayrollDetail> companyPayrollDetails = new ArrayList<>();
        CompanyPayrollDetail companyPayrollDetail;

        XSSFWorkbook wb = new XSSFWorkbook(file);
        XSSFSheet sheet = wb.getSheetAt(0);

        int excelIndex = 1;
        Row row;
        System.out.println("Reading Payroll File . . . ");
        for (int i = 1; i < sheet.getLastRowNum() + 1; i++) {
            companyPayrollDetail = new CompanyPayrollDetail();
            row = sheet.getRow(i);

            companyPayrollDetail.setDestAccount(xlsxReaderUtil.readStringCell(row, 0));
            companyPayrollDetail.setAmount(new BigDecimal(xlsxReaderUtil.readNumericCell(row, 1)));
            companyPayrollDetail.setEmpId(xlsxReaderUtil.readStringCell(row, 2));
            companyPayrollDetail.setEmpName(xlsxReaderUtil.readStringCell(row, 3));
            companyPayrollDetail.setEmpDept(xlsxReaderUtil.readStringCell(row, 4));
            companyPayrollDetail.setPaymentDate(xlsxReaderUtil.readDateCell(row, 5));
            companyPayrollDetail.setCompanyPayrollId(companyPayroll);

            companyPayrollDetail.setStatus("hehe");

            companyPayrollDetails.add(companyPayrollDetail);
            excelIndex++;

            insertCompanyPayrollDetail(companyPayrollDetail);
        }

        return companyPayrollDetails;
    }
}
