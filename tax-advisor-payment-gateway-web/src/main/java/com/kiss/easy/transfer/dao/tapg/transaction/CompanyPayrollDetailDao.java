package com.kiss.easy.transfer.dao.tapg.transaction;

import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayrollDetail;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.CountQuery;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.UpdateQuery;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Component
public class CompanyPayrollDetailDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public long countByCompanyPayrollId(String companyPayrollId){
        return queryBuilder
                .count()
                .from(CountQuery.from(CompanyPayrollDetail.class, "t0"))
                .where(CountQuery.condition("t0", "companyPayrollId", EquationTypeEnum.EQUALS_IGNORECASE, companyPayrollId))
                .execute();
    }

    public int insertCompanyPayroll(CompanyPayrollDetail companyPayrollDetail){
        return queryBuilder
                .insert(companyPayrollDetail.getClass())
                .setParameter(companyPayrollDetail)
                .execute();
    }

    public CompanyPayrollDetail selectOneById(String id){
        return queryBuilder
                .select()
                .from(SelectQuery.from(CompanyPayrollDetail.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(CompanyPayrollDetail.class);
    }

    public List<CompanyPayrollDetail> selectAllByCompanyPayrollId(String companyPayrollId){
        return queryBuilder
                .select()
                .from(SelectQuery.from(CompanyPayrollDetail.class, "t0"))
                .where(SelectQuery.condition("t0", "companyPayrollId", EquationTypeEnum.EQUALS_IGNORECASE, companyPayrollId))
                .asList(CompanyPayrollDetail.class);
    }
}
