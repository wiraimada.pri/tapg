package com.kiss.easy.transfer.config;

import com.kiss.easy.transfer.authhandler.CsrfAccessDeniedHandler;
import com.kiss.easy.transfer.controller.authenticated.base.BaseAuthenticatedController;
import com.kiss.easy.transfer.controller.permitted.LoginController;
import com.kiss.easy.transfer.enumeration.UserTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Created by daniel on 12/19/16.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userService;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private CsrfAccessDeniedHandler csrfAccessDeniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers(BaseAuthenticatedController.PATH_BASE + "**").hasAnyAuthority(UserTypeEnum.USER.name())
			.antMatchers(
                "/**",
                "/"
            ).permitAll()
            .anyRequest().fullyAuthenticated()
            .and()
                .exceptionHandling()
                .accessDeniedHandler(csrfAccessDeniedHandler)
            .and()
                .sessionManagement()
                .invalidSessionUrl(LoginController.PATH_LOGIN_ERROR_SESSION_EXPIRED)
            .and()
                .formLogin()
                .loginPage(LoginController.PATH_LOGIN)
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .permitAll()
        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }
}