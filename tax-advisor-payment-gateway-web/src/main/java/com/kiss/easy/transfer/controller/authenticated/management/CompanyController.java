package com.kiss.easy.transfer.controller.authenticated.management;

import com.kiss.easy.transfer.constant.FieldConstant;
import com.kiss.easy.transfer.constant.StringConstant;
import com.kiss.easy.transfer.controller.authenticated.base.BaseManagementController;
import com.kiss.easy.transfer.entity.tapg.master.Company;
import com.kiss.easy.transfer.enumeration.MenuEnum;
import com.kiss.easy.transfer.service.tapg.master.CompanyService;
import com.kiss.easy.transfer.util.FormValidationUtil;
import com.kiss.pojo.DataTablesSearchResult;
import com.kiss.pojo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CompanyController extends BaseManagementController {
    /********************************************** - SERVICE SECTION - **********************************************/
    @Autowired
    private CompanyService companyService;

    /********************************************** - AUTO INJECT SECTION - **********************************************/
    @Autowired
    private FormValidationUtil formValidationUtil;

    /********************************************** - ROOT PATH SECTION - **********************************************/
    public static final String PAGE_DIRECTORY = PAGE_BASE + "company/";
    public static final String PATH_ROOT = PATH_BASE + "company";

    /********************************************** - LIST SECTION - **********************************************/
    public static final String PAGE_LIST = PAGE_DIRECTORY + "list";
    public static final String PATH_LIST = PATH_ROOT;

    public static final String COMMAND_LIST = "redirect:" + PATH_LIST;

    @GetMapping({PATH_LIST, PATH_LIST + "/*"})
    public String doShowListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");
        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("activeMenu", MenuEnum.MANAGEMENT_COMPANY.id());

        return getPageContent(PAGE_LIST);
    }

    /********************************************** - AJAX SEARCH LIST SECTION - **********************************************/
    public static final String PATH_AJAX_SEARCH_LIST = PATH_ROOT + "/search";

    @PostMapping({PATH_AJAX_SEARCH_LIST})
    public void doSearchDataTablesList(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesSearchResult dataTablesSearchResult = new DataTablesSearchResult();
            try {
                dataTablesSearchResult.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesSearchResult.setDraw(0);
            }

            int offset = 0;
            int limit = 50;
            try {
                offset = Math.abs(Integer.parseInt(request.getParameter("start")));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Math.abs(Integer.parseInt(request.getParameter("length")));
            } catch (NumberFormatException e) {
            }

            List<String[]> data = new ArrayList<>();
            SearchResult<Company> searchResult = companyService.selectAllCompaniesByLoggedInUser();

            if (searchResult.getCount() > 0) {
                dataTablesSearchResult.setRecordsFiltered(searchResult.getCount());
                dataTablesSearchResult.setRecordsTotal(searchResult.getCount());

                int i = offset;
                String labelEdit = getMessage("button.edit");
                for (Company result : searchResult.getList()) {
                    data.add(new String[]{
                            "" + (++i),
                            result.getName(),
                            result.getEmail(),
                            result.isActive() ? "Aktif" : "Tidak Aktif",
                            "<div class=\"btn-group btn-group-xs\" role=\"group\">" +
                                    "<a href=\"" + getPathURL(PATH_EDIT + "/" + result.getId()) + "\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i> <span>" + labelEdit + "</span></a>" +
                                    "</div>"
                    });
                }
            }
            dataTablesSearchResult.setData(data);

            writeJsonResponse(response, dataTablesSearchResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /********************************************** - ADD SECTION - **********************************************/
    public static final String PAGE_ADD = PAGE_DIRECTORY + "add";
    public static final String PATH_ADD = PATH_ROOT + "/add";
    private static final String REQUEST_MAPPING_ADD = PATH_ADD + "*";
    public static final String COMMAND_ADD = "redirect:" + PATH_ADD;

    @GetMapping(REQUEST_MAPPING_ADD)
    public String doShowAddPage(final Model model, final HttpServletRequest request) {

        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");

        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }

        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("data", new Company());

        return getPageContent(PAGE_ADD);
    }

    @PostMapping(value = PATH_ADD)
    public String doAdd(@ModelAttribute("data") Company company, final BindingResult errors, final Model model, final HttpServletRequest requestl) {
        try {

            formValidationUtil.validatingInput(errors, "name", company.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);
            formValidationUtil.validatingOptionalInput(errors, "name", company.getVaNumber(), getMessage("label.vaNumber"), FieldConstant.MAX_LENGTH_VA_NUMBER);
            formValidationUtil.validatingEmail(errors, "email", company.getEmail(), getMessage("label.email"), FieldConstant.MAX_LENGTH_EMAIL);

            if (!errors.hasErrors()) {
//            ========================INSERT NEW DATA=========================
                companyService.insertCompany(company);
                model.addAttribute("okMessage", getMessage("message.ok.data"));
                return getPageContent(PAGE_LIST);
            } else {
                model.addAttribute("errorMessage", getMessage("message.error.data"));
                return getPageContent(PAGE_ADD);
            }

        } catch (Exception e) {
            String errMessage = null;
            errMessage = getMessage("message.error.data");
            e.printStackTrace();
            model.addAttribute("errorMessage", errMessage);
            return getPageContent(PAGE_ADD);
        }

    }

    /********************************************** - EDIT SECTION - **********************************************/
    public static final String PAGE_EDIT = PAGE_DIRECTORY + "edit";
    public static final String PATH_EDIT = PATH_ROOT + "/edit";
    private static final String REQUEST_MAPPING_EDIT = PATH_EDIT + "/{id}";
    public static final String COMMAND_EDIT = "redirect:" + PATH_EDIT;

    @GetMapping(value = REQUEST_MAPPING_EDIT)
    public String doShowEditPage(@PathVariable("id") String id, final Model model, final HttpServletRequest request) {
        Company company = companyService.selectOneCompanyById(id);

        if (company == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.company.edit.invalid");
        }
        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("data", company);

        return getPageContent(PAGE_EDIT);
    }

    @PostMapping(REQUEST_MAPPING_EDIT)
    public String doEdit(@ModelAttribute("company") final Company company, final BindingResult errors, final Model model, final HttpServletRequest request, final HttpServletResponse response) {

        String id = request.getParameter("id");
        Company companyOriginal = companyService.selectOneCompanyById(id);

        if (companyOriginal == null) {
            return getPageContent(PAGE_LIST) + "?errorMessage=" + getMessage("form.error.company.edit.invalid");
        }

        company.setId(companyOriginal.getId());

        formValidationUtil.validatingInput(errors, "name", company.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);
        formValidationUtil.validatingOptionalInput(errors, "name", company.getVaNumber(), getMessage("label.vaNumber"), FieldConstant.MAX_LENGTH_VA_NUMBER);
        formValidationUtil.validatingEmail(errors, "email", company.getEmail(), getMessage("label.email"), FieldConstant.MAX_LENGTH_EMAIL);
//        formValidationUtil.validatingMandatoryInput(errors, "isActive", String.valueOf(company.getIsActive()), getMessage("label.email"));


        if (!errors.hasErrors()) {
            try {
                companyOriginal.setName(company.getName());
                companyOriginal.setVaNumber(company.getVaNumber());
                companyOriginal.setEmail(company.getEmail());
                companyOriginal.setActive(company.isActive());
                companyService.updateCompany(companyOriginal);
                String okMessage = getMessage("form.ok.master.company.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", companyOriginal.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_EDIT);
            }
        } else return getPageContent(PAGE_EDIT) + "?errorMessage=" + getMessage("message.error.data");

    }

    /********************************************** - GLOBAL MODEL ATTRIBUTE SECTION - **********************************************/
    @ModelAttribute
    public void addModelAttribute(Model model) {
        model.addAttribute("title", getMessage("page.title.management.company"));
    }
}
