package com.kiss.easy.transfer.constant;

/**
 * Created by NEVIIM - DIX on 15/01/2019.
 */
public interface FieldConstant {
    /***************************** - COMMON - *****************************/
    short ID_LENGTH = 36;
    short MAX_LENGTH_EMAIL_ADDRESS = 50;
    short MAX_LENGTH_NAME = 50;
    short MAX_LENGTH_REASON = 1000;

    /***************************** - COMPANY - *****************************/
    short MAX_LENGTH_VA_NUMBER = 20;


    /***************************** - USER - *****************************/
    short MAX_LENGTH_EMAIL = 255;
    short MAX_LENGTH_PASSWORD = 255;
    short MAX_LENGTH_USERNAME = 50;
}
