package com.kiss.easy.transfer.service.tapg_admin;

import com.kiss.easy.transfer.dao.tapg_admin.master.TaCompanyDao;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.easy.transfer.service.tapg.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dixio69 on 24/01/2019 in the 'easy-transfer' Project.
 */
@Service
public class TaCompanyService extends BaseService{
    @Autowired
    private TaCompanyDao taCompanyDao;

    public TaCompany selectCurrentTaCompany(){
        return selectOne(getLoggedUser().getTaCompany().getId());
    }

    public TaCompany selectOne(String id){
        return taCompanyDao.selectOne(id);
    }
}
