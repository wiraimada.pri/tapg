package com.kiss.easy.transfer.controller.authenticated.management;

import com.kiss.easy.transfer.constant.FieldConstant;
import com.kiss.easy.transfer.controller.authenticated.base.BaseManagementController;
import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.RoleMenu;
import com.kiss.easy.transfer.enumeration.MenuEnum;
import com.kiss.easy.transfer.service.tapg.master.RoleMenuService;
import com.kiss.easy.transfer.service.tapg.master.RoleService;
import com.kiss.easy.transfer.service.tapg.statics.MenuService;
import com.kiss.easy.transfer.util.FormValidationUtil;
import com.kiss.pojo.AjaxCallResult;
import com.kiss.pojo.DataTablesSearchResult;
import com.kiss.pojo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Controller
public class RoleController extends BaseManagementController {
    /********************************************** - SERVICE SECTION - **********************************************/
    @Autowired
    private RoleMenuService roleMenuService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;

    /********************************************** - AUTO INJECT SECTION - **********************************************/
    @Autowired
    private FormValidationUtil formValidationUtil;

    /********************************************** - ROOT PATH SECTION - **********************************************/
    private static final String PAGE_DIRECTORY = PAGE_BASE + "role/";
    public static final String PATH_ROOT = PATH_BASE + "role";

    /********************************************** - LIST SECTION - **********************************************/
    public static final String PAGE_LIST = PAGE_DIRECTORY + "list";
    public static final String PATH_LIST = PATH_ROOT;
    private static final String REQUEST_MAPPING_LIST = PATH_LIST + "/*";
    public static final String COMMAND_LIST = "redirect:" + PATH_LIST;

    @GetMapping({PATH_LIST, REQUEST_MAPPING_LIST})
    public String doShowListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");
        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("activeMenu", MenuEnum.MANAGEMENT_ROLE.id());

        return getPageContent(PAGE_LIST);
    }

    /********************************************** - AJAX SEARCH LIST SECTION - **********************************************/
    public static final String PATH_AJAX_SEARCH_LIST = PATH_ROOT + "/search";
    private static final String REQUEST_MAPPING_SEARCH_LIST = PATH_AJAX_SEARCH_LIST + "*";

    @PostMapping({REQUEST_MAPPING_SEARCH_LIST})
    public void doSearchDataTablesList(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesSearchResult dataTablesSearchResult = new DataTablesSearchResult();
            try {
                dataTablesSearchResult.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesSearchResult.setDraw(0);
            }
            int offset = 0;
            int limit = 50;
            try {
                offset = Math.abs(Integer.parseInt(request.getParameter("start")));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Math.abs(Integer.parseInt(request.getParameter("length")));
            } catch (NumberFormatException e) {
            }

            List<String[]> data = new ArrayList<>();
            SearchResult<Role> searchResult = roleService.selectAllRoleForLoggedInUser();
            if (searchResult.getCount() > 0) {
                dataTablesSearchResult.setRecordsFiltered(searchResult.getCount());
                dataTablesSearchResult.setRecordsTotal(searchResult.getCount());

                String labelEdit = getMessage("label.edit");
                String labelAssignMenu = getMessage("label.menu");
                int i = offset;
                for (Role result : searchResult.getList()) {
                    data.add(new String[]{
                            "" + (++i),
                            result.getName(),
                            (
                                    "<a href=\"" + getPathURL(PATH_SET_MENU + "/" + result.getId()) + "\" class=\"btn btn-sm btn-default\">" + labelAssignMenu + "</a> " +
                                            "<a href=\"" + getPathURL(PATH_EDIT + "/" + result.getId()) + "\" class=\"btn btn-sm btn-warning\">" + labelEdit + "</a> "
                            )
                    });
                }
            }
            dataTablesSearchResult.setData(data);

            writeJsonResponse(response, dataTablesSearchResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /********************************************** - ADD SECTION - **********************************************/
    public static final String PAGE_ADD = PAGE_DIRECTORY + "add";
    public static final String PATH_ADD = PATH_ROOT + "/add";
    private static final String REQUEST_MAPPING_ADD = PATH_ADD + "*";
    public static final String COMMAND_ADD = "redirect:" + PATH_ADD;

    @RequestMapping(value = REQUEST_MAPPING_ADD, method = {RequestMethod.GET})
    public String doShowAddPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {

        String okMessage = request.getParameter("okMessage");
        String errorMessage = request.getParameter("errorMessage");

        if (okMessage != null && !okMessage.equalsIgnoreCase("")) {
            model.addAttribute("okMessage", okMessage);
        }

        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("form", new Role());

        return getPageContent(PAGE_ADD);
    }

    @RequestMapping(value = REQUEST_MAPPING_ADD, method = {RequestMethod.POST})
    public String doAdd(@ModelAttribute("form") final Role role, final BindingResult errors, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            formValidationUtil.validatingInput(errors, "name", role.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);

            if (!errors.hasErrors()) {
//            ========================INSERT NEW DATA=========================
                roleService.insertRole(role);
                model.addAttribute("okMessage", getMessage("message.ok.data"));
                return getPageContent(PAGE_LIST);
            } else {
                model.addAttribute("form", role);
                model.addAttribute("errorMessage", getMessage("message.error.data"));
                return getPageContent(PAGE_ADD);
            }
        } catch (Exception e) {
            String errMessage = null;
            errMessage = getMessage("message.error.data");
            e.printStackTrace();
            model.addAttribute("errorMessage", errMessage);
            return getPageContent(PAGE_ADD);
        }
    }

    /********************************************** - DELETE SECTION - **********************************************/
    public static final String PATH_AJAX_DELETE = PATH_ROOT + "/delete";
    private static final String REQUEST_MAPPING_AJAX_DELETE = PATH_AJAX_DELETE + "*";

    @RequestMapping(value = REQUEST_MAPPING_AJAX_DELETE, method = {RequestMethod.POST})
    public void doDelete(final HttpServletRequest request, final HttpServletResponse response) {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            String[] targetArr = request.getParameterValues("target[]");
            String[] targetArrName = request.getParameterValues("targetName[]");

            AjaxCallResult generalAjaxCallResult = new AjaxCallResult(getMessage("message.error.handler.invalid.request"));
            try {
                roleService.deleteRole(targetArr);

                generalAjaxCallResult.setOk(true);
                generalAjaxCallResult.setMessage(targetArrName.toString());
            } catch (NumberFormatException e) {
                generalAjaxCallResult.setMessage(getMessage("form.error.delete", targetArrName.toString()));
                if (!isProduction()) {
                    logger.error(e.getMessage(), e);
                }
            } catch (DataIntegrityViolationException e) {
                generalAjaxCallResult.setMessage(getMessage("form.error.delete", targetArrName.toString()));
                if (!isProduction()) {
                    logger.error(e.getMessage(), e);
                }
            }
            writeJsonResponse(response, generalAjaxCallResult);
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    /********************************************** - EDIT SECTION - **********************************************/
    public static final String PAGE_EDIT = PAGE_DIRECTORY + "edit";
    public static final String PATH_EDIT = PATH_ROOT + "/edit";
    private static final String REQUEST_MAPPING_EDIT = PATH_EDIT + "/{id}";
    public static final String COMMAND_EDIT = "redirect:" + PATH_EDIT;

    @RequestMapping(value = REQUEST_MAPPING_EDIT, method = {RequestMethod.GET})
    public String doShowEditPage(final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        Role roleOriginal = roleService.selectOne(id);

        if (roleOriginal == null) {
            throw new Exception();
//            throw new NoHandlerFoundException(RequestMethod.POST.name(), request.getRequestURL().toString(), headers);
        }
        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }

        model.addAttribute("form", roleOriginal);

        return getPageContent(PAGE_EDIT);
    }

    @RequestMapping(value = REQUEST_MAPPING_EDIT, method = {RequestMethod.POST})
    public String doEdit(@ModelAttribute("form") final Role role, final BindingResult errors, final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        Role roleOriginal = roleService.selectOne(id);

        if (roleOriginal == null)
            throw new Exception();
//            throw new NoHandlerFoundException(RequestMethod.POST.name(), request.getRequestURL().toString(), headers);
        if (role.getRowVersion() != roleOriginal.getRowVersion()) {
            model.addAttribute("errorMessage", getMessage("form.error.role.edit.invalid"));
            return getPageContent(PAGE_LIST);
        }

        role.setId(roleOriginal.getId());

        formValidationUtil.validatingInput(errors, "name", role.getName(), getMessage("label.name"), FieldConstant.MAX_LENGTH_NAME);

        if (!errors.hasErrors()) {
            try {
                roleOriginal.setName(role.getName());
                roleService.updateRole(roleOriginal);

                String okMessage = getMessage("form.ok.master.role.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", roleOriginal.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_EDIT);
            }
        } else return getPageContent(PAGE_EDIT) + "?errorMessage=" + getMessage("message.error.data");
    }

    /********************************************** - SET-MENU SECTION - **********************************************/
    public static final String PAGE_SET_MENU = PAGE_DIRECTORY + "set-menu";
    public static final String PATH_SET_MENU = PATH_ROOT + "/set-menu";
    private static final String REQUEST_MAPPING_ASSIGN_MENU = PATH_SET_MENU + "/{id}";

    @RequestMapping(value = REQUEST_MAPPING_ASSIGN_MENU, method = {RequestMethod.GET})
    public String doShowAssignMenuPage(final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        Role originalRole = roleService.selectOne(id);

        if (originalRole == null) {
            getPageContent(RoleController.PAGE_LIST);
        }

        String errorMessage = request.getParameter("errorMessage");
        if (errorMessage != null && !errorMessage.equalsIgnoreCase("")) {
            model.addAttribute("errorMessage", errorMessage);
        }


        List<RoleMenu> roleMenuTemp = roleMenuService.selectAllRoleMenu();
        if (originalRole.getListRoleMenuId().size() > 0) {
            for (RoleMenu rm : roleMenuTemp) {
                rm.setSelected(originalRole.getListRoleMenuId().contains(rm.getMenu().getId()));
            }
        }

        originalRole.setRoleMenus(roleMenuTemp);
        model.addAttribute("form", originalRole);

        return getPageContent(PAGE_SET_MENU);
    }

    @RequestMapping(value = REQUEST_MAPPING_ASSIGN_MENU, method = {RequestMethod.POST})
    public String doAssignMenu(@ModelAttribute("form") final Role role, final BindingResult errors, final @PathVariable("id") String id, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        Role originalRole = roleService.selectOne(id);

        if (originalRole == null) {
            getPageContent(PAGE_LIST);
        }

        if (role.getRowVersion() != originalRole.getRowVersion()) {
            model.addAttribute("errorMessage", getMessage("form.error.user.edit.invalid"));
            return getPageContent(PAGE_LIST);
        }

        role.setId(originalRole.getId());
        String[] menuArr = request.getParameterValues("menuArr[]");

        if (!errors.hasErrors()) {
            try {
                roleMenuService.updateRoleMenu(role, menuArr);

                String okMessage = getMessage("form.ok.master.role.edit");
                model.addAttribute("okMessage", okMessage);
                return getPageContent(PAGE_LIST);
            } catch (Exception e) {
                String errMessage = null;
                errMessage = getMessage("message.error.data");
                e.printStackTrace();
                model.addAttribute("id", originalRole.getId());
                model.addAttribute("errorMessage", errMessage);
                return getPageContent(PAGE_SET_MENU);
            }
        } else {
            model.addAttribute("errorMessage", getMessage("message.error.data"));
            return getPageContent(PAGE_SET_MENU);
        }
    }

    /********************************************** - GLOBAL MODEL ATTRIBUTE SECTION - **********************************************/
    @ModelAttribute
    public void addModelAttribute(final Model model, final HttpServletRequest request) {
        if (!isAjaxRequest(request)) {
            model.addAttribute("title", getMessage("page.title.management.role"));
        }
    }
}
