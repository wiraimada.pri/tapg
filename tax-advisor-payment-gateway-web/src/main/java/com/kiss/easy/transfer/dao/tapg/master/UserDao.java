package com.kiss.easy.transfer.dao.tapg.master;

import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.enumeration.UserTypeEnum;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.CountQuery;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.UpdateQuery;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 14/01/2019.
 */
@Component
public class UserDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public List<User> selectAll(){
        return queryBuilder
                .select()
                .from(SelectQuery.from(User.class, "t0"))
                .asList(User.class);
    }

    public List<User> selectAllByTaCompanyId(String taCompanyId){
        return queryBuilder
                .select()
                .from(SelectQuery.from(User.class, "t0"))
                .where(SelectQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS_IGNORECASE, taCompanyId))
                .asList(User.class);
    }

    public User selectOneById(String id){
        return queryBuilder
                .select()
                .from(SelectQuery.from(User.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(User.class);
    }

    public User selectOneByUsername(String username){
        return queryBuilder
                .select()
                .from(SelectQuery.from(User.class, "t0"))
                .where(
                        SelectQuery.condition("t0", "username", EquationTypeEnum.EQUALS_IGNORECASE, username)
                                .and(SelectQuery.condition("t0", "userType.name", EquationTypeEnum.EQUALS_IGNORECASE, UserTypeEnum.USER.name()))
                )
                .asObject(User.class);
    }

    public int insertUser(User user){
        return queryBuilder
                .insert(user.getClass())
                .setParameter(user)
                .execute();
    }

    public int updateUser(User user) {
        return queryBuilder
                .update(user.getClass())
                .setParameter(user)
                .where(UpdateQuery.condition("id", EquationTypeEnum.EQUALS, user.getId()))
                .execute();
    }

    public long count(){
        return queryBuilder
                .count()
                .from(CountQuery.from(User.class, "t0"))
                .execute();
    }

    public long countByTaCompanyId(String taCompanyId){
        return queryBuilder
                .count()
                .from(CountQuery.from(User.class, "t0"))
                .where(CountQuery.condition("t0", "taCompany", EquationTypeEnum.EQUALS, taCompanyId))
                .execute();
    }

    public int deleteUser(String id) {
        return queryBuilder
                .delete(User.class)
                .where(DeleteQuery.condition("id", EquationTypeEnum.EQUALS, id))
                .execute();
    }
}
