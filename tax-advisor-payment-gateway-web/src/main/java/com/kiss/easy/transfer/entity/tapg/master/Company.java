package com.kiss.easy.transfer.entity.tapg.master;

import com.kiss.easy.transfer.entity.MasterEntity;
import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.m_company")
public class Company extends MasterEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "name")
    private String name;
    @Column(name = "va_number")
    private String vaNumber;
    @Column(name = "email")
    private String email;
    @Column(name = "is_active")
    private boolean active;
    @Column(name = "m_ta_company_id", referenceField = "id")
    private TaCompany taCompany;
    /***************************** - TRANSIENT FIELD SECTION - ****************************/

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {return active;}

    public void setActive(boolean active) {
        this.active = active;
    }

    public TaCompany getTaCompany() {
        return taCompany;
    }

    public void setTaCompany(TaCompany taCompany) {
        this.taCompany = taCompany;
    }

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
}
