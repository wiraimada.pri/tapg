package com.kiss.easy.transfer.dao.tapg.master;

import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.master.UserRole;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.CountQuery;
import com.kiss.mybatis.query.QueryBuilder.JoinTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRoleDao {
	@Autowired
	private QueryBuilder queryBuilder;

	public int deleteUserRole(String id) {
		return queryBuilder
				.delete(UserRole.class)
				.where(DeleteQuery.condition("id", EquationTypeEnum.EQUALS, id))
				.execute();
	}

	public int insertUserRole(UserRole userRole) {
		return queryBuilder
				.insert(userRole.getClass())
				.setParameter(userRole)
				.execute();
	}

	public List<Role> selectAllRoles(User user) {
		return queryBuilder.select("t1.*")
				.from(SelectQuery.from(UserRole.class, "t0"))
				.join(SelectQuery.join(JoinTypeEnum.JOIN, Role.class, "t1", SelectQuery.condition("t0", "role", EquationTypeEnum.EQUALS, "t1", "id"), null))
				.where(SelectQuery.condition("t0", "user", EquationTypeEnum.EQUALS, user.getId()))
				.orderBy(SelectQuery.orderByAscending("t1", "name"))
				.asList(Role.class);
	}

	public List<UserRole> selectAllUserRolesByUserId(String userId) {
		return queryBuilder
				.select()
				.from(SelectQuery.from(UserRole.class, "t0"))
				.where(SelectQuery.condition("t0", "user", EquationTypeEnum.EQUALS_IGNORECASE, userId))
				.asList(UserRole.class);
	}

	public List<UserRole> selectAllUserRolesByTaCompanyId(String taCompanyId) {
		return null;
	}

	public long countUserRolesByUserId(String userId) {
		return queryBuilder
				.count()
				.from(CountQuery.from(UserRole.class, "t0"))
				.where(CountQuery.condition("t0", "user", EquationTypeEnum.EQUALS, userId))
				.execute();
	}
}
