package com.kiss.easy.transfer.controller.permitted;

import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.web.thymeleaf.annotation.Decorator;
import com.kiss.web.thymeleaf.controller.BaseController;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by daniel on 5/28/15.
 */
@Controller
public class HttpErrorHandlerController extends BaseController implements ErrorController {
	public static final String PATH_ERROR = "/error";

	public static final String REQUEST_MAPPING_ERROR = PATH_ERROR + "*";

	@Override
	public String getErrorPath() {
		return PATH_ERROR;
	}

	@Decorator("error")
	@RequestMapping(REQUEST_MAPPING_ERROR)
	public String handleHttpError500(final Model model, final HttpServletRequest request, final Exception e) {
		logger.error(e.getMessage(), e);

		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		int statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
		if (status != null) {
			try {
				statusCode = Integer.valueOf(status.toString());
			} catch (NumberFormatException e1) {
			}
		}

		if (statusCode == HttpStatus.FORBIDDEN.value()) {
			model.addAttribute("title", getMessage("page.title.error.handler.access.forbidden"));
			model.addAttribute("titleDetail", getMessage("page.title.error.handler.access.forbidden.detail"));
			model.addAttribute("errorMessage", getMessage("message.error.handler.access.forbidden", request.getAttribute("javax.servlet.forward.request_uri")));
		} else if (statusCode == HttpStatus.NOT_FOUND.value()) {
			model.addAttribute("title", getMessage("page.title.error.handler.page.not.found"));
			model.addAttribute("titleDetail", getMessage("page.title.error.handler.page.not.found.detail"));
			model.addAttribute("errorMessage", getMessage("message.error.handler.page.not.found", request.getAttribute("javax.servlet.forward.request_uri")));
		} else {
			model.addAttribute("title", getMessage("page.title.error.handler.unexpected.exception"));
			model.addAttribute("titleDetail", getMessage("page.title.error.handler.unexpected.exception.detail"));
			model.addAttribute("errorMessage", getMessage("message.error.handler.unexpected.exception", request.getAttribute("javax.servlet.forward.request_uri")));
		}
		model.addAttribute("statusCode", statusCode);

		Object object = (Object) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (object != null && object instanceof User) {
			User user = (User) object;

			String activeMenu = "";

			StringBuffer sbMenu = new StringBuffer();
			if (user.getMenuList() != null) {
				for (Menu menu : user.getMenuList()) {
					sbMenu.append(renderMenu(menu, activeMenu));
				}
			}
			model.addAttribute("menu", sbMenu.toString());
		}


		return getErrorPage("httpError");
	}

	private String renderMenu(Menu menu, String activeMenu) {
		StringBuffer sbMenu = new StringBuffer();
		if (menu.hasChildren()) {
			sbMenu.append(
					"<li class=\"treeview" + (activeMenu.startsWith(menu.getId()) ? " active" : "") + "\">" +
						"<a href=\"javascript:void(0)\">" +
							"<i class=\"fa " + menu.getIcon() + "\"></i> <span>" + getMessage(menu.getLabel()) + "</span>" +
							"<span class=\"pull-right-container\">" +
								"<i class=\"fa fa-angle-left pull-right\"></i>" +
							"</span>" +
						"</a>" +
						"<ul class=\"treeview-menu\">"
			);
			for (Menu childMenu : menu.getChildMenuList()) {
				sbMenu.append(renderMenu(childMenu, activeMenu));
			}
			sbMenu.append(
						"</ul>" +
					"</li>"
			);

		} else {
			sbMenu.append(
					"<li" + (menu.getId().equalsIgnoreCase(activeMenu) ? " class=\"active\"" : "") + ">" +
						"<a href=\"" + (menu.getUrl() == null ? "javascript:void(0)" : getPathURL(menu.getUrl())) + "\">" +
							"<i class=\"fa " + menu.getIcon() + "\"></i> <span>" + getMessage(menu.getLabel()) + "</span>" +
						"</a>" +
					"</li>"
			);
		}
		return sbMenu.toString();
	}
}
