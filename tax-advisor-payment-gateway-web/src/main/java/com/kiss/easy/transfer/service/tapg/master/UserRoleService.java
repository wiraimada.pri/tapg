package com.kiss.easy.transfer.service.tapg.master;

import com.kiss.easy.transfer.dao.tapg.master.UserRoleDao;
import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.master.UserRole;
import com.kiss.easy.transfer.service.tapg.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dixio69 on 22/01/2019 in the 'easy-transfer' Project.
 */
@Service
public class UserRoleService extends BaseService {

    @Autowired
    private UserRoleDao userRoleDao;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;

    @Transactional
    public void updateUserRole(User user, String[] roleArr) {
        UserRole userRole;
        User originalUser = userService.selectOneUserById(user.getId());
        // to update user role, simply delete existing data and write new
        for (UserRole ur : originalUser.getUserRoles()) {
            userRoleDao.deleteUserRole(ur.getId());
        }
        if (roleArr != null && roleArr.length > 0)
            for (String roleId : roleArr) {
                userRole = new UserRole();
                prepareInsert(userRole);
                userRole.setUser(originalUser);
                userRole.setRole(roleService.selectOne(roleId));
                userRoleDao.insertUserRole(userRole);
            }
    }


    public List<UserRole> selectAllUserRolesByTaCompanyId(String taCompanyId) {
        return userRoleDao.selectAllUserRolesByTaCompanyId(taCompanyId);
    }

    public List<UserRole> selectAllUserRolesForLoggedInUser() {
        List<Role> roles = roleService.selectAllRoleForLoggedInUser().getList();
        List<UserRole> userRoles = new ArrayList<>();
        for (Role r : roles) {
            userRoles.add(new UserRole(r));
        }
        return userRoles;
    }
}
