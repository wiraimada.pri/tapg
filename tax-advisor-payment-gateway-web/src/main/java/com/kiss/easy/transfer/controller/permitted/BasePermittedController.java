package com.kiss.easy.transfer.controller.permitted;

import com.kiss.web.thymeleaf.controller.BaseController;

public abstract class BasePermittedController extends BaseController {
	protected static final String DIRECTORY_PAGE = "permitted/";
}
