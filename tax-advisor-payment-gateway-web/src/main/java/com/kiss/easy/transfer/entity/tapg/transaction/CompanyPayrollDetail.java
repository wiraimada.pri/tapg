package com.kiss.easy.transfer.entity.tapg.transaction;

import com.kiss.easy.transfer.entity.BaseEntity;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.trx_company_payroll_detail")
public class CompanyPayrollDetail extends BaseEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "trx_company_payroll_id", referenceField = "id")
    private CompanyPayroll companyPayrollId;
    @Column(name = "status")
    private String status;
    @Column(name = "reason")
    private String reason;
    @Column(name = "payment_date")
    private Timestamp paymentDate;
    @Column(name = "dest_account")
    private String destAccount;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "emp_id")
    private String empId;
    @Column(name = "emp_name")
    private String empName;
    @Column(name = "emp_dept")
    private String empDept;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public CompanyPayroll getCompanyPayrollId() {
        return companyPayrollId;
    }

    public void setCompanyPayrollId(CompanyPayroll companyPayrollId) {
        this.companyPayrollId = companyPayrollId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Timestamp getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Timestamp paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDestAccount() {
        return destAccount;
    }

    public void setDestAccount(String destAccount) {
        this.destAccount = destAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpDept() {
        return empDept;
    }

    public void setEmpDept(String empDept) {this.empDept = empDept;}

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
}
