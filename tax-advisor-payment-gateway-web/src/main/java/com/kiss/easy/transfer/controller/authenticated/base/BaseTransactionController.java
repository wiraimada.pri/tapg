package com.kiss.easy.transfer.controller.authenticated.base;

/**
 * Created by dixio69 on 18/01/2019 in the 'easy-transfer' Project.
 */
public abstract class BaseTransactionController extends BaseAuthenticatedController {
    protected static final String PAGE_TRANSACTION = BaseAuthenticatedController.PAGE_BASE + "transaction/";

    public static final String PATH_TRANSACTION = BaseAuthenticatedController.PATH_BASE + "transaction/";
}
