package com.kiss.easy.transfer.dao.tapg.transaction;

import com.kiss.easy.transfer.entity.tapg.transaction.CompanyPayroll;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.CountQuery;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.UpdateQuery;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Created by NEVIIM - DIX on 11/01/2019.
 */
@Component
public class CompanyPayrollDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public long count(){
        return queryBuilder
                .count()
                .from(CountQuery.from(CompanyPayroll.class, "t0"))
                .execute();
    }

    public long countByCompanyId(String companyId){
        return queryBuilder
                .count()
                .from(CountQuery.from(CompanyPayroll.class, "t0"))
                .where(CountQuery.condition("t0", "companyId", EquationTypeEnum.EQUALS_IGNORECASE, companyId))
                .execute();
    }

    public int insertCompanyPayroll(CompanyPayroll companyPayroll){
        return queryBuilder
                .insert(companyPayroll.getClass())
                .setParameter(companyPayroll)
                .execute();
    }

    public List<CompanyPayroll> selectAll(){
        return queryBuilder
                .select()
                .from(SelectQuery.from(CompanyPayroll.class, "t0"))
                .asList(CompanyPayroll.class);
    }

    public List<CompanyPayroll> selectAllByCompanyId(String companyId){
        return queryBuilder
                .select()
                .from(SelectQuery.from(CompanyPayroll.class, "t0"))
                .where(SelectQuery.condition("t0", "companyId", EquationTypeEnum.EQUALS_IGNORECASE, companyId))
                .asList(CompanyPayroll.class);
    }

    public CompanyPayroll selectOneById(String id){
        return queryBuilder
                .select()
                .from(SelectQuery.from(CompanyPayroll.class, "t0"))
                .where(SelectQuery.condition("t0", "id", EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(CompanyPayroll.class);
    }

    public int updateCompanyPayroll(CompanyPayroll companyPayroll) {
        return queryBuilder
                .update(companyPayroll.getClass())
                .setParameter(companyPayroll)
                .where(UpdateQuery.condition("id", EquationTypeEnum.EQUALS, companyPayroll.getId()))
                .execute();
    }

}
