package com.kiss.easy.transfer.entity.tapg.master;

import com.kiss.easy.transfer.entity.BaseEntity;
import com.kiss.easy.transfer.entity.shared.Access;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.m_role_menu")
public class RoleMenu extends BaseEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "m_role_id", referenceField = "id")
    private Role role;
    @Column(name = "s_menu_id", referenceField = "id")
    private Menu menu;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/
    private boolean selected=false;

    /***************************** - CONSTRUCTOR SECTION - ****************************/
    public RoleMenu() {}

    public RoleMenu(Menu menu) {this.menu = menu;}

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/
    public boolean isSelected() {return selected;}

    public void setSelected(boolean selected) {this.selected = selected;}

    /***************************** - OTHER METHOD SECTION - ****************************/
}
