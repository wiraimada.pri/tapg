package com.kiss.easy.transfer.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Created by NEVIIM - DIX on 16/01/2019.
 */
@Component
public class XlsxReaderUtil {

    public Timestamp readDateCell(Row row, int cellNo) {
        if (row.getCell(cellNo) == null || row.getCell(cellNo).getCellType() == Cell.CELL_TYPE_BLANK) {
            return null;
        } else {
            return new Timestamp(row.getCell(cellNo).getDateCellValue().getTime());
        }
    }

    public Double readNumericCell(Row row, int cellNo) {
        if (row.getCell(cellNo) == null || row.getCell(cellNo).getCellType() == Cell.CELL_TYPE_BLANK) {
            return null;
        } else {
            return row.getCell(cellNo).getNumericCellValue();
        }
    }

    public String readStringCell(Row row, int cellNo) {
        if (row.getCell(cellNo) == null || row.getCell(cellNo).getCellType() == Cell.CELL_TYPE_BLANK) {
            return "";
        } else {
            return row.getCell(cellNo).getStringCellValue();
        }
    }

    public String readStringNumericCell(Row row, int cellNo) {
        if (row.getCell(cellNo) == null || row.getCell(cellNo).getCellType() == Cell.CELL_TYPE_BLANK) {
            return "";
        } else {
            row.getCell(cellNo).setCellType(CellType.STRING);
            String cellValue = row.getCell(cellNo).getCellType() == CellType.NUMERIC.getCode() ?
                    row.getCell(cellNo).getNumericCellValue() + "" : row.getCell(cellNo).getStringCellValue();
            return cellValue;
        }
    }
}
