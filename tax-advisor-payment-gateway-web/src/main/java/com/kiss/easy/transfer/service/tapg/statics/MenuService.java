package com.kiss.easy.transfer.service.tapg.statics;

import com.kiss.easy.transfer.dao.tapg.statics.MenuDao;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.easy.transfer.service.tapg.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dixio69 on 21/01/2019 in the 'easy-transfer' Project.
 */
@Service
public class MenuService extends BaseService{
    @Autowired
    private MenuDao menuDao;

    public List<Menu> selectAllMenu(){
        return menuDao.selectAll();
    }

    public Menu selectOneMenu(String id){
        return menuDao.selectOneById(id);
    }
}
