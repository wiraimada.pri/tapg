package com.kiss.easy.transfer.service.tapg.master;

import com.kiss.easy.transfer.dao.tapg.master.RoleMenuDao;
import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.RoleMenu;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.easy.transfer.service.tapg.BaseService;
import com.kiss.easy.transfer.service.tapg.statics.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dixio69 on 21/01/2019 in the 'easy-transfer' Project.
 */
@Service
public class RoleMenuService extends BaseService {

    @Autowired
    private RoleMenuDao roleMenuDao;
    @Autowired
    private RoleMenuService roleMenuService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private RoleService roleService;

    @Transactional
    public void updateRoleMenu(Role role, String[] menuArr) {
        RoleMenu roleMenu;
        Role originalRole = roleService.selectOne(role.getId());
        // to update role menu, simply delete existing data and write new
        for (RoleMenu rm : originalRole.getRoleMenus()) {
            roleMenuDao.deleteRoleMenu(rm.getId());
        }
        if (menuArr != null && menuArr.length > 0)
            for (String menuId : menuArr) {
                roleMenu = new RoleMenu();
                prepareInsert(roleMenu);
                roleMenu.setRole(originalRole);
                roleMenu.setMenu(menuService.selectOneMenu(menuId));
                roleMenuDao.insertRoleMenu(roleMenu);
            }
    }

    public List<RoleMenu> selectAllRoleMenu() {
        List<Menu> menus = menuService.selectAllMenu();
        List<RoleMenu> roleMenus = new ArrayList<>();
        for (Menu m : menus) {
            roleMenus.add(new RoleMenu(m));
        }
        return roleMenus;
    }

    public List<RoleMenu> selectAllRoleMenuByRoleId(String roleId) {
        return roleMenuDao.selectAllRoleMenuByRoleId(roleId);
    }
}
