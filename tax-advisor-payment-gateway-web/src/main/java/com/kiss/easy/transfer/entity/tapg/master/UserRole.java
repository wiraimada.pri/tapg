package com.kiss.easy.transfer.entity.tapg.master;

import com.kiss.easy.transfer.entity.BaseEntity;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.util.UUID;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.m_user_role")
public class UserRole extends BaseEntity {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "m_user_id", referenceField = "id")
    private User user;
    @Column(name = "m_role_id", referenceField = "id")
    private Role role;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/
    private boolean selected;
    /***************************** - CONSTRUCTOR SECTION - ****************************/

    public UserRole() {}

    public UserRole(Role role) {setRole(role);}

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /***************************** - OTHER METHOD SECTION - ****************************/
}
