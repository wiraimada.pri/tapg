package com.kiss.easy.transfer.util;

import com.kiss.util.FieldValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 15/01/2019.
 */
@Component
public final class FormValidationUtil {

    @Autowired
    private FieldValidationUtil fieldValidationUtil;

    public void validatingEmail(BindingResult errors, String name, String value, String label, short maxLength) {
        label = label.toLowerCase();
        if (fieldValidationUtil.required(errors, name, value, label)) {
            if (fieldValidationUtil.maxLength(errors, name, value, label, maxLength)){
                fieldValidationUtil.email(errors, name, value, label);
            }
        }
    }

    public void validatingInput(BindingResult errors, String name, String value, String label, short maxLength) {
        label = label.toLowerCase();
        if (fieldValidationUtil.required(errors, name, value, label)) {
            fieldValidationUtil.maxLength(errors, name, value, label, maxLength);
        }
    }

//    public void validatingInputRole(BindingResult errors, String name, UserGroup value, String label, List<UserGroup> valueList) {
//        label = label.toLowerCase();
//        if (fieldValidationUtil.required(errors, name, value, label)) {
//            fieldValidationUtil.maxLength(errors, name, value, label, maxLength);
//        }
//    }

    public void validatingMandatoryInput(BindingResult errors, String name, String value, String label) {
        label = label.toLowerCase();
        fieldValidationUtil.required(errors, name, value, label);
    }

    public void validatingMatchedValue(BindingResult errors, String name, String value, String label, String matchValue,String matchLabel) {
        label = label.toLowerCase();
        fieldValidationUtil.match(errors, name, value, label, matchValue, matchLabel);
    }

    public void validatingOptionalInput(BindingResult errors, String name, String value, String label, short maxLength) {
        label = label.toLowerCase();
        fieldValidationUtil.maxLength(errors, name, value, label, maxLength);
    }
}
