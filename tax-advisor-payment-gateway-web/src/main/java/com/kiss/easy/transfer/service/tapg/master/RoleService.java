package com.kiss.easy.transfer.service.tapg.master;

import com.kiss.easy.transfer.dao.tapg.master.RoleDao;
import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.service.tapg.BaseService;
import com.kiss.pojo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dixio69 on 21/01/2019 in the 'easy-transfer' Project.
 */
@Service
public class RoleService extends BaseService{
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private RoleMenuService roleMenuService;

    @Transactional
    public void deleteRole(String id) {
        roleDao.deleteRole(id);
    }

    @Transactional
    public void deleteRole(String[] id) {
        for (String s : id) {
            roleDao.deleteRole(s);
        }
    }

    public void insertRole(Role role){
        role.setTaCompany(getLoggedUser().getTaCompany());
        prepareInsert(role);
        roleDao.insertRole(role);
    }

    public SearchResult<Role> selectAllRoleForLoggedInUser() {
        return selectAllRoleByTaCompanyId(getLoggedUser().getTaCompany().getId());
    }

    public SearchResult<Role> selectAllRoleByTaCompanyId(String taCompanyId) {
        SearchResult<Role> searchResult = new SearchResult<>();
        searchResult.setCount((int) roleDao.countByTaCompanyId(taCompanyId));
        if (searchResult.getCount() > 0) {
            searchResult.setList(roleDao.selectAllByTaCompanyId(taCompanyId));
        }
        return searchResult;
    }

    public Role selectOne(String id){
        Role role = roleDao.selectOneById(id);
        role.setRoleMenus(roleMenuService.selectAllRoleMenuByRoleId(role.getId()));
        return role;
    }

    @Transactional
    public void updateRole(Role role) {
        prepareUpdate(role);
        roleDao.updateRole(role);
    }
}
