package com.kiss.easy.transfer.entity.tapg.statics;

import com.kiss.easy.transfer.entity.shared.Access;
import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg.s_menu")
public class Menu extends Access {
	/***************************** - FIELD SECTION - ****************************/
	@Column(name = "parent_menu_id", referenceField = "id")
	private Menu parentMenu;

	/***************************** - TRANSIENT FIELD SECTION - ****************************/
	private List<Menu> childMenuList;

	/***************************** - CONSTRUCTOR SECTION - ****************************/
	public Menu() {
	}

	public Menu(String id) {
		this.setId(id);
	}

	/***************************** - GETTER SETTER METHOD SECTION - ****************************/
	public Menu getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(Menu parentMenu) {
		this.parentMenu = parentMenu;
	}

	/***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/
	public List<Menu> getChildMenuList() {
		return this.childMenuList;
	}

	/***************************** - OTHER METHOD SECTION - ****************************/
	public void addChildMenu(Menu menu) {
		if (this.childMenuList == null) {
			this.childMenuList = new ArrayList<>();
		}
		this.childMenuList.add(menu);
	}

	public boolean hasChildren() {
		return (this.childMenuList != null && this.childMenuList.size() > 0);
	}
}
