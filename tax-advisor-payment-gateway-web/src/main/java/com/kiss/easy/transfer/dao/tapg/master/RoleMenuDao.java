package com.kiss.easy.transfer.dao.tapg.master;

import com.kiss.easy.transfer.entity.tapg.master.Role;
import com.kiss.easy.transfer.entity.tapg.master.RoleMenu;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.mybatis.query.QueryBuilder;
import com.kiss.mybatis.query.QueryBuilder.EquationTypeEnum;
import com.kiss.mybatis.query.QueryBuilder.SelectQuery;
import com.kiss.mybatis.query.QueryBuilder.DeleteQuery;
import com.kiss.mybatis.query.QueryBuilder.JoinTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RoleMenuDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public int deleteRoleMenu(String id) {
        return queryBuilder
                .delete(RoleMenu.class)
                .where(DeleteQuery.condition("id", EquationTypeEnum.EQUALS, id))
                .execute();
    }

    public int insertRoleMenu(RoleMenu roleMenu) {
        return queryBuilder
                .insert(roleMenu.getClass())
                .setParameter(roleMenu)
                .execute();
    }

    public List<RoleMenu> selectAll(List<Role> roleList) {
        List<String> roleIdList = new ArrayList<>();
        for (Role role : roleList) {
            roleIdList.add(role.getId());
        }
        return queryBuilder
                .select()
                .from(SelectQuery.from(RoleMenu.class, "t0"))
                .join(SelectQuery.join(JoinTypeEnum.JOIN, Menu.class, "t1", SelectQuery.condition("t0", "menu", EquationTypeEnum.EQUALS, "t1", "id"), "menu"))
                .where(SelectQuery.condition("t0", "role", EquationTypeEnum.IN, roleIdList))
                .orderBy(SelectQuery.orderByAscending("t1", "level"), SelectQuery.orderByAscending("t1", "order"))
                .asList(RoleMenu.class);
    }

    public List<RoleMenu> selectAllRoleMenuByRoleId(String roleId) {
        return queryBuilder
                .select()
                .from(SelectQuery.from(RoleMenu.class, "t0"))
                .where(SelectQuery.condition("t0", "role", EquationTypeEnum.EQUALS_IGNORECASE, roleId))
                .asList(RoleMenu.class);
    }

}
