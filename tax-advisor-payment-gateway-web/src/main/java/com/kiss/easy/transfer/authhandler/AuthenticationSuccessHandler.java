package com.kiss.easy.transfer.authhandler;

import com.kiss.easy.transfer.controller.authenticated.HomeController;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.service.tapg.master.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component("authenticationSuccessHandler")
public class AuthenticationSuccessHandler implements org.springframework.security.web.authentication.AuthenticationSuccessHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Value("${user.session.timeout:300}")
    private int userSessionTimeout;

    @Autowired
    private UserService userService;

    @Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
		if (response.isCommitted()) {
			return;
		}

	    request.getSession().setMaxInactiveInterval(userSessionTimeout);

        User user = (User) auth.getPrincipal();
        user.setRoleList(userService.selectAllRoles(user));
	    if (user.getRoleList() != null && user.getRoleList().size() > 0) {
		    user.setRoleMenuList(userService.selectAllRoleMenuList(user.getRoleList()));
	    }

        redirectStrategy.sendRedirect(request, response, HomeController.PATH_HOME);
	}
}