package com.kiss.easy.transfer.enumeration;

import com.kiss.easy.transfer.entity.tapg.statics.Menu;

/**
 * Created by daniel on 1/22/19.
 */
public enum MenuEnum {
	HOME,
	MANAGEMENT,
	MANAGEMENT_COMPANY,
	MANAGEMENT_ROLE,
	MANAGEMENT_USER,
	TRANSACTION,
	TRANSACTION_APPROVAL,
	TRANSACTION_UPLOAD,
	;

	public Menu value() {
		return new Menu(id());
	}

	public String id() {
		return name().replace("_", ".");
	}
}
