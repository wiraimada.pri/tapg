package com.kiss.easy.transfer.interceptor;

import com.kiss.easy.transfer.controller.authenticated.base.BaseAuthenticatedController;
import com.kiss.easy.transfer.entity.tapg.master.User;
import com.kiss.easy.transfer.entity.tapg.statics.Menu;
import com.kiss.web.thymeleaf.interceptor.KissInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.PathMatcher;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 1/20/19.
 */
@Component("authenticatedUserInterceptor")
public class AuthenticatedUserInterceptor extends KissInterceptor implements ServletContextAware {
	private ServletContext servletContext;

	@Autowired
	private MessageSourceAccessor messages;

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
		if (response.isCommitted()) {
			return;
		}
		if (modelAndView != null) {
			Object object = (Object) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if (object != null && object instanceof User) {
				User user = (User) object;

				String activeMenu = (String) modelAndView.getModel().get("activeMenu");
				if (activeMenu == null) {
					activeMenu = "";
				}

				StringBuffer sbMenu = new StringBuffer();
				if (user.getMenuList() != null) {
					for (Menu menu : user.getMenuList()) {
						sbMenu.append(renderMenu(menu, activeMenu));
					}
				}
				modelAndView.addObject("menu", sbMenu.toString());
			}
		}
	}

	@Override
	public String[] getAddPathPatterns() {
		return new String[]{BaseAuthenticatedController.PATH_BASE + "**"};
	}

	@Override
	public String[] getExcludePathPatterns() {
		return null;
	}

	@Override
	public PathMatcher getPathMatcher() {
		return null;
	}

	private String renderMenu(Menu menu, String activeMenu) {
		StringBuffer sbMenu = new StringBuffer();
		if (menu.hasChildren()) {
			sbMenu.append(
					"<li class=\"treeview" + (activeMenu.startsWith(menu.getId()) ? " active" : "") + "\">" +
						"<a href=\"javascript:void(0)\">" +
							"<i class=\"fa " + menu.getIcon() + "\"></i> <span>" + this.messages.getMessage(menu.getLabel()) + "</span>" +
							"<span class=\"pull-right-container\">" +
								"<i class=\"fa fa-angle-left pull-right\"></i>" +
							"</span>" +
						"</a>" +
						"<ul class=\"treeview-menu\">"
			);
			for (Menu childMenu : menu.getChildMenuList()) {
				sbMenu.append(renderMenu(childMenu, activeMenu));
			}
			sbMenu.append(
						"</ul>" +
					"</li>"
			);

		} else {
			sbMenu.append(
					"<li" + (menu.getId().equalsIgnoreCase(activeMenu) ? " class=\"active\"" : "") + ">" +
						"<a href=\"" + (menu.getUrl() == null ? "javascript:void(0)" : this.servletContext.getContextPath() + menu.getUrl()) + "\">" +
							"<i class=\"fa " + menu.getIcon() + "\"></i> <span>" + this.messages.getMessage(menu.getLabel()) + "</span>" +
						"</a>" +
					"</li>"
			);
		}
		return sbMenu.toString();
	}
}
