function showLoading() {
	$('body').prepend('<div class="loading"><i class="fas fa-spinner fa-spin fa-6x"></i></div>');
}
function hideLoading() {
	$('div.loading').detach();
}