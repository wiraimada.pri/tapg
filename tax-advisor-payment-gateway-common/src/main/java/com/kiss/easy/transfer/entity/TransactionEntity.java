package com.kiss.easy.transfer.entity;

import com.kiss.mybatis.annotation.Column;

import java.sql.Timestamp;

public abstract class TransactionEntity extends BaseEntity {
	/***************************** - FIELD SECTION - ****************************/
	@Column(name = "created_by", referenceField = "id")
	private UserEntity createdBy;
	@Column(name = "created_date")
	private Timestamp createdDate;

	/***************************** - TRANSIENT FIELD SECTION - ****************************/

	/***************************** - CONSTRUCTOR SECTION - ****************************/

	/***************************** - GETTER SETTER METHOD SECTION - ****************************/
	public UserEntity getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserEntity createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/***************************** - OTHER METHOD SECTION - ****************************/
}
