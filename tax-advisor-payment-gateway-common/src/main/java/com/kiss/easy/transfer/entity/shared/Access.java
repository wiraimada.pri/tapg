package com.kiss.easy.transfer.entity.shared;

import com.kiss.mybatis.annotation.Column;

public class Access {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "id")
    private String id;
    @Column(name = "label")
    private String label;
    @Column(name = "url")
    private String url;
    @Column(name = "level")
    private String level;
    @Column(name = "order")
    private String order;
    @Column(name = "icon")
    private String icon;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getId() {return id;}

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	/***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
}
