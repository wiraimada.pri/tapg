package com.kiss.easy.transfer.entity.shared;

import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
public class Authority implements GrantedAuthority, Serializable {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "name")
    private String name;

	/***************************** - TRANSIENT FIELD SECTION - ****************************/

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    public Authority() {
    }

    public Authority(String name) {
        this.name = name;
    }

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
    public String getAuthority() {
        return getName();
    }
}
