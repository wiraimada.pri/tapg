package com.kiss.easy.transfer.dao.tapg_admin.master;

import com.kiss.easy.transfer.entity.tapg_admin.master.TaCompany;
import com.kiss.mybatis.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NEVIIM - DIX on 15/01/2019.
 */
@Component
public class TaCompanyDao {
    @Autowired
    private QueryBuilder queryBuilder;

    public long count() {
        return queryBuilder
                .count()
                .from(QueryBuilder.CountQuery.from(TaCompany.class, "t0"))
                .execute();
    }

    public long countActive() {
        return queryBuilder
                .count()
                .from(QueryBuilder.CountQuery.from(TaCompany.class, "t0"))
                .where(QueryBuilder.CountQuery.condition("t0", "active", QueryBuilder.EquationTypeEnum.EQUALS, true))
                .execute();
    }

    public List<TaCompany> selectAll(){
        return queryBuilder
                .select()
                .from(QueryBuilder.SelectQuery.from(TaCompany.class, "t0"))
                .asList(TaCompany.class);
    }

    public List<TaCompany> selectAllActive(){
        return queryBuilder
                .select()
                .from(QueryBuilder.SelectQuery.from(TaCompany.class, "t0"))
                .where(QueryBuilder.SelectQuery.condition("t0", "active", QueryBuilder.EquationTypeEnum.EQUALS, true))
                .asList(TaCompany.class);
    }

    public TaCompany selectOne(String id){
        return queryBuilder
                .select()
                .from(QueryBuilder.SelectQuery.from(TaCompany.class, "t0"))
                .where(QueryBuilder.SelectQuery.condition("t0", "id", QueryBuilder.EquationTypeEnum.EQUALS_IGNORECASE, id))
                .asObject(TaCompany.class);
    }

    public TaCompany selectOneDummy() {
        return queryBuilder
                .select()
                .from(QueryBuilder.SelectQuery.from(TaCompany.class, "t0"))
                .where(QueryBuilder.SelectQuery.condition("t0", "id", QueryBuilder.EquationTypeEnum.EQUALS_IGNORECASE, "X-001"))
                .asObject(TaCompany.class);
    }
}
