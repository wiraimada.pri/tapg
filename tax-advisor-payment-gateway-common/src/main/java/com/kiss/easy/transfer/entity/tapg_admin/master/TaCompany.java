package com.kiss.easy.transfer.entity.tapg_admin.master;

import com.kiss.mybatis.annotation.Column;
import com.kiss.mybatis.annotation.Entity;

import java.util.UUID;

/**
 * Created by NEVIIM - DIX on 10/01/2019.
 */
@Entity(name = "tapg_admin.m_ta_company")
public class TaCompany {
    /***************************** - FIELD SECTION - ****************************/
    @Column(name = "id")
    private String id;
    @Column(name = "name")
	private String name;
    @Column(name = "is_active")
    private boolean active;
    @Column(name = "code")
    private String code;

    /***************************** - TRANSIENT FIELD SECTION - ****************************/

    /***************************** - CONSTRUCTOR SECTION - ****************************/

    /***************************** - GETTER SETTER METHOD SECTION - ****************************/
    public String getId() {
        if (id == null || id.equals("")) {
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCode() {return code;}

    public void setCode(String code) {this.code = code;}

    /***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

    /***************************** - OTHER METHOD SECTION - ****************************/
}
