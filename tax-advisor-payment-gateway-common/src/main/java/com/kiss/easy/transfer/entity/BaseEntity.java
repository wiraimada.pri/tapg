package com.kiss.easy.transfer.entity;

import com.kiss.mybatis.annotation.Column;

public class BaseEntity {
	/***************************** - FIELD SECTION - ****************************/
	@Column(name = "id")
	private String id;

	/***************************** - TRANSIENT FIELD SECTION - ****************************/

	/***************************** - CONSTRUCTOR SECTION - ****************************/

	/***************************** - GETTER SETTER METHOD SECTION - ****************************/
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/***************************** - GETTER SETTER TRANSIENT METHOD SECTION - *****************************/

	/***************************** - OTHER METHOD SECTION - ****************************/
}
