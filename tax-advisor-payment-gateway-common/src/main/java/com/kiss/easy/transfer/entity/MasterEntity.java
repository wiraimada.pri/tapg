package com.kiss.easy.transfer.entity;

import com.kiss.mybatis.annotation.Column;

import java.sql.Timestamp;

public abstract class MasterEntity extends BaseEntity {
	/***************************** - FIELD SECTION - ****************************/
	@Column(name = "created_by", referenceField = "id")
	private UserEntity createdBy;
	@Column(name = "created_date")
	private Timestamp createdDate;
	@Column(name = "last_updated_by", referenceField = "id")
	private UserEntity lastUpdatedBy;
	@Column(name = "last_updated_date")
	private Timestamp lastUpdatedDate;
	@Column(name = "row_version")
	private double rowVersion = 1.0;

	/***************************** - TRANSIENT FIELD SECTION - ****************************/

	/***************************** - CONSTRUCTOR SECTION - ****************************/

	/***************************** - GETTER SETTER METHOD SECTION - ****************************/
	public UserEntity getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserEntity createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public UserEntity getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(UserEntity lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public double getRowVersion() {
		return this.rowVersion;
	}

	public void setRowVersion(double rowVersion) {
		this.rowVersion = rowVersion;
	}

	/***************************** - OTHER METHOD SECTION - ****************************/
//	public String getHash() {
//		try {
//			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
//			messageDigest.update(getId());
//		} catch (NoSuchAlgorithmException e) {
//		}
//
//		return null;
//	}
}
