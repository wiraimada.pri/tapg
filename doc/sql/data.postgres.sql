SET search_path = public;
DROP EXTENSION IF EXISTS "uuid-ossp";
CREATE EXTENSION "uuid-ossp";

-- CHANGE SCHEMA TO TAPG ADMIN --
SET search_path TO tapg_admin, public;

insert into tapg_admin.s_user_type ("name") values ('SYSTEM'), ('USER');

insert into tapg_admin.m_user (id, username, "password", "name", email, s_user_type_name, is_enabled, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, created_by, created_date, row_version) values ('SYSTEM', 'system', null, 'SYSTEM', 'no-reply@tapg.com', 'SYSTEM', false, false, false, false, null, NOW(), 0);
insert into tapg_admin.m_user (id, username, "password", "name", email, s_user_type_name, is_enabled, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, created_by, created_date, row_version) values (public.uuid_generate_v4(), 'admin', '$2a$10$gEcMUp8clbL9hs2CJfKC3upAG8CxvuYWB/TFgRLFyi4oY0uUCGDXG', 'Administrator', 'admin@tapg.com', 'USER', true, true, true, true, 'SYSTEM', NOW(), 1);
insert into tapg_admin.m_ta_company (id, code, "name", is_active, created_by, created_date, row_version) values (public.uuid_generate_v4(), 'smco', 'SMCO', true, 'SYSTEM', NOW(), 1.0);

-- CHANGE SCHEMA TO TAPG --
SET search_path TO tapg, public;

insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('HOME', 'menu.home', '/home', 0, 0, 'fa-home', null);
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('MANAGEMENT', 'menu.management', null, 0, 1, 'fa-gear', null);
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('MANAGEMENT.USER', 'menu.management.user', '/admin/management/user', 1, 0, 'fa-users', 'MANAGEMENT');
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('MANAGEMENT.COMPANY', 'menu.management.company', '/admin/management/company', 1, 1, 'fa-building-o', 'MANAGEMENT');
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('MANAGEMENT.ROLE', 'menu.management.role', '/admin/management/role', 1, 1, 'fa-building-o', 'MANAGEMENT');
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('TRANSACTION', 'menu.transaction', null, 0, 2, 'fa-money', null);
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('TRANSACTION.UPLOAD', 'menu.transaction.upload', '/admin/transaction/upload', 1, 0, 'fa-file-excel-o', 'TRANSACTION');
insert into tapg.s_menu (id,	label, url, "level", "order", icon, parent_menu_id) values ('TRANSACTION.APPROVAL', 'menu.transaction.approval', '/admin/transaction/approval', 1, 1, 'fa-check', 'TRANSACTION');

insert into tapg.s_user_type ("name") values ('SYSTEM'), ('USER');

insert into tapg.m_user (id, username, "password", "name", email, s_user_type_name, is_enabled, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, m_ta_company_id, created_by, created_date, row_version) values ('SYSTEM', 'system', null, 'SYSTEM', 'no-reply@tapg.com', 'SYSTEM', false, false, false, false, null, null, NOW(), 0);
insert into tapg.m_user (id, username, "password", "name", email, s_user_type_name, is_enabled, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, m_ta_company_id, created_by, created_date, row_version) values (public.uuid_generate_v4(), 'smco_admin', '$2a$10$gEcMUp8clbL9hs2CJfKC3upAG8CxvuYWB/TFgRLFyi4oY0uUCGDXG', 'SMCO Administrator', 'admin@smco.com', 'USER', true, true, true, true, (SELECT id FROM tapg_admin.m_ta_company WHERE code = 'smco' LIMIT 1), 'SYSTEM', NOW(), 1);

insert INTO tapg.m_role(id, m_ta_company_id, "name", created_by, created_date, row_version) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg_admin.m_ta_company WHERE code = 'smco' LIMIT 1), 'smco_administrator', 'SYSTEM', NOW(), 1);

insert INTO tapg.m_role_menu(id, m_role_id, s_menu_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1), 'HOME');
insert INTO tapg.m_role_menu(id, m_role_id, s_menu_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1), 'MANAGEMENT');
insert INTO tapg.m_role_menu(id, m_role_id, s_menu_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1), 'MANAGEMENT.USER');
insert INTO tapg.m_role_menu(id, m_role_id, s_menu_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1), 'MANAGEMENT.COMPANY');
insert INTO tapg.m_role_menu(id, m_role_id, s_menu_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1), 'MANAGEMENT.ROLE');

insert into tapg.m_user_role(id, m_user_id, m_role_id) VALUES (public.uuid_generate_v4(), (SELECT id FROM tapg.m_user WHERE username = 'smco_admin'), (SELECT id FROM tapg.m_role WHERE "name" = 'smco_administrator' LIMIT 1));