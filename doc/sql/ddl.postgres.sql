/* TAPG Admin Schema */
CREATE DATABASE "easy-transfer";

DROP SCHEMA IF EXISTS tapg_admin CASCADE;
CREATE SCHEMA tapg_admin;
SET search_path TO tapg_admin;

CREATE TABLE tapg_admin.s_menu (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	label VARCHAR(50) NOT NULL,
	url VARCHAR(255),
	"level" INT NOT NULL,
	"order" INT NOT NULL,
	"icon" VARCHAR(20),
	parent_menu_id VARCHAR(36) REFERENCES tapg_admin.s_menu (id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX ON tapg_admin.s_menu (label);
CREATE UNIQUE INDEX ON tapg_admin.s_menu (url);

CREATE TABLE tapg_admin.s_user_type (
	"name" VARCHAR(10) NOT NULL PRIMARY KEY
);

CREATE TABLE tapg_admin.m_user (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	username VARCHAR(50) NOT NULL,
	"password" VARCHAR(255),
	"name" VARCHAR(50) NOT NULL,
	email VARCHAR(255) NOT NULL,
	s_user_type_name VARCHAR(10) NOT NULL REFERENCES tapg_admin.s_user_type("name") ON UPDATE CASCADE ON DELETE RESTRICT,
	is_enabled BOOLEAN NOT NULL,
	is_account_non_expired BOOLEAN NOT NULL,
	is_account_non_locked BOOLEAN NOT NULL,
	is_credentials_non_expired BOOLEAN NOT NULL,
	created_by VARCHAR(36) REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE UNIQUE INDEX ON tapg_admin.m_user (username);

CREATE TABLE tapg_admin.m_role (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	"name" VARCHAR(50) NOT NULL,
	created_by VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE UNIQUE INDEX ON tapg_admin.m_role ("name");

CREATE TABLE tapg_admin.m_role_menu (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	m_role_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_role (id) ON UPDATE CASCADE ON DELETE CASCADE,
	s_menu_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.s_menu (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE tapg_admin.m_user_role (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	m_user_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_user (id) ON UPDATE CASCADE ON DELETE CASCADE,
	m_role_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_role (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tapg_admin.m_ta_company (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	code VARCHAR(10) NOT NULL,
	"name" VARCHAR(255) NOT NULL,
	is_active BOOLEAN NOT NULL,
	created_by VARCHAR(36) REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg_admin.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE UNIQUE INDEX ON tapg_admin.m_ta_company (code);

CREATE TABLE tapg_admin.m_ta_company_info_bca (
	ta_company_id VARCHAR(36) NOT NULL PRIMARY KEY REFERENCES tapg_admin.m_ta_company(id) ON UPDATE CASCADE ON DELETE CASCADE,
	client_id VARCHAR(36) NOT NULL,
	client_secret VARCHAR(5000) NOT NULL
);
CREATE UNIQUE INDEX ON tapg_admin.m_ta_company_info_bca (client_id);

/* TAPG Schema */
DROP SCHEMA IF EXISTS tapg CASCADE;
CREATE SCHEMA tapg;
SET search_path TO tapg;

CREATE TABLE tapg.s_menu (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	label VARCHAR(50) NOT NULL,
	url VARCHAR(255),
	"level" INT NOT NULL,
	"order" INT NOT NULL,
	"icon" VARCHAR(20),
	parent_menu_id VARCHAR(36) REFERENCES tapg.s_menu (id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX ON tapg.s_menu (label);
CREATE UNIQUE INDEX ON tapg.s_menu (url);

CREATE TABLE tapg.s_user_type (
	"name" VARCHAR(10) NOT NULL PRIMARY KEY
);

CREATE TABLE tapg.m_user (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	"password" VARCHAR(255),
	"name" VARCHAR(50) NOT NULL,
	email VARCHAR(255) NOT NULL,
	s_user_type_name VARCHAR(10) NOT NULL REFERENCES tapg.s_user_type("name") ON UPDATE CASCADE ON DELETE RESTRICT,
	is_enabled BOOLEAN NOT NULL,
	is_account_non_expired BOOLEAN NOT NULL,
	is_account_non_locked BOOLEAN NOT NULL,
	is_credentials_non_expired BOOLEAN NOT NULL,
	m_ta_company_id VARCHAR(36) REFERENCES tapg_admin.m_ta_company(id),
	created_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE UNIQUE INDEX ON tapg.m_user (username);

CREATE TABLE tapg.m_role (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	"name" VARCHAR(255) NOT NULL,
	m_ta_company_id VARCHAR(36) REFERENCES tapg_admin.m_ta_company(id),
	created_by VARCHAR(36) NOT NULL REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE UNIQUE INDEX ON tapg.m_role ("name");

CREATE TABLE tapg.m_role_menu (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	m_role_id VARCHAR(36) NOT NULL REFERENCES tapg.m_role (id) ON UPDATE CASCADE ON DELETE CASCADE,
	s_menu_id VARCHAR(36) NOT NULL REFERENCES tapg.s_menu (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE tapg.m_user_role (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	m_user_id VARCHAR(36) NOT NULL REFERENCES tapg.m_user (id) ON UPDATE CASCADE ON DELETE CASCADE,
	m_role_id VARCHAR(36) NOT NULL REFERENCES tapg.m_role (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE tapg.m_company (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	m_ta_company_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_ta_company(id),
	"name" VARCHAR(255) NOT NULL,
	va_number VARCHAR(20),
	email VARCHAR(50) NOT NULL,
	is_active BOOLEAN NOT NULL,
	created_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	last_updated_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	last_updated_date TIMESTAMP,
	row_version DOUBLE PRECISION NOT NULL
);
CREATE INDEX ON tapg.m_company (va_number);

CREATE TABLE tapg.trx_company_payroll (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	"status" VARCHAR(10) NOT NULL,
	reason VARCHAR(1000),
	m_ta_company_id VARCHAR(36) NOT NULL REFERENCES tapg_admin.m_ta_company(id),
	m_company_id VARCHAR(36) NOT NULL REFERENCES tapg.m_company(id),
	periode_year INT NOT NULL,
	periode_month INT NOT NULL,
	created_by VARCHAR(36) NOT NULL REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	created_date TIMESTAMP NOT NULL,
	checked_by VARCHAR(36) REFERENCES tapg.m_user ON UPDATE CASCADE ON DELETE RESTRICT,
	checked_date TIMESTAMP
);

CREATE TABLE tapg.trx_company_payroll_detail (
	id VARCHAR(36) NOT NULL PRIMARY KEY,
	trx_company_payroll_id VARCHAR(36) NOT NULL REFERENCES tapg.trx_company_payroll(id),
	"status" VARCHAR(10) NOT NULL,
	reason VARCHAR(1000),
	payment_date TIMESTAMP NOT NULL,
	dest_account VARCHAR(20) NOT NULL,
	amount DOUBLE PRECISION NOT NULL,
	emp_id VARCHAR(255),
	emp_name VARCHAR(255),
	emp_dept VARCHAR(255)
);