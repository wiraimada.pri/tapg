function clickToggle() {
    localStorage.setItem('tampungToggle', (localStorage.getItem('tampungToggle') == 'true') ? 'false' : 'true');
}

if (localStorage.getItem('tampungToggle') == 'true') {
    $("body").addClass('sidebar-open sidebar-collapse')
} else if(localStorage.getItem('tampungToggle') == 'false' || !localStorage.getItem('tampungToggle') == undefined) {
    $("body").removeClass('sidebar-open sidebar-collapse')
}